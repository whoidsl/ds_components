/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#ifndef ABORT_SUPERVISOR_H
#define ABORT_SUPERVISOR_H

#include <ds_base/ds_process.h>
#include <ds_base/BoolCommand.h>
#include <ds_core_msgs/Abort.h>
#include <ds_core_msgs/VoidCmd.h>
#include <ds_core_msgs/CountdownMonitor.h>
#include <ds_core_msgs/CriticalProcess.h>
#include <ds_hotel_msgs/AbortCmd.h>
#include <boost/algorithm/string.hpp>

namespace abort_supervisor
{
struct AbortSupervisorPrivate;
class AbortSupervisor : public ds_base::DsProcess
{
  DS_DECLARE_PRIVATE(AbortSupervisor)

public:
  AbortSupervisor();
  AbortSupervisor(int argc, char* argv[], const std::string& name);
  ~AbortSupervisor() override;
  DS_DISABLE_COPY(AbortSupervisor)

  void publishAbortMessage();
  void publishCountdownMessage();

  void updateTtl(const ds_core_msgs::CriticalProcess& msg);

protected:
  void setupPublishers() override;
  void setupServices() override;
  void setupTimers() override;
  void setupParameters() override;
  void setupSubscriptions() override;

  void stopAbortRefreshTimer();

  void scanCriticalProcesses();

  // Pure virtual function that the derived classes need to implement
  // To define the actions to be taken during abort
  virtual void executeAbort() = 0;

  bool _abort_cmd(ds_hotel_msgs::AbortCmd::Request req, ds_hotel_msgs::AbortCmd::Response resp);

  bool _timer_off(ds_core_msgs::VoidCmd::Request req, ds_core_msgs::VoidCmd::Response resp);

  bool _rescan_params_cmd(ds_base::BoolCommand::Request req, ds_base::BoolCommand::Response resp);

  bool _abort(void);

private:
  std::unique_ptr<AbortSupervisorPrivate> d_ptr_;
};
}
#endif  // ABORT_SUPERVISOR_H
