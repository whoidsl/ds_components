/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#ifndef ABORT_SUPERVISOR_PRIVATE_H
#define ABORT_SUPERVISOR_PRIVATE_H

#include "abort_supervisor/abort_supervisor.h"
#include "ds_core_msgs/Abort.h"

namespace abort_supervisor
{
struct AbortSupervisorPrivate
{
  bool ticking_shortman_;
  ros::Publisher abort_pub_;
  ros::Timer abort_refresh_timer_;
  ds_core_msgs::Abort msg_;
  ros::ServiceServer stop_ticking_shortman_cmd_;
  ros::ServiceServer abort_cmd_;
  ros::ServiceServer rescan_params_cmd_;
  // aborting_ is true if the abort procedure is already being executed
  bool aborting_;
  // Countdown monitor for critical processes
  std::unordered_map<std::string, ros::Time> monitor_expirations_;
  int n_critical_processes_;

  ros::Subscriber critical_process_sub_;
  ros::Timer countdown_refresh_timer_;
  ros::Publisher countdown_pub_;
  ds_core_msgs::CountdownMonitor cm_;
};
}

#endif  // ABORT_SUPERVISOR_PRIVATE_H
