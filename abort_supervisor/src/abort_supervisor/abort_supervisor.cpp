/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by zac on 2/6/18.
//

#include "abort_supervisor/abort_supervisor.h"
#include "abort_supervisor_private.h"

#include <ds_core_msgs/Abort.h>
#include <ds_hotel_msgs/AbortCmd.h>

namespace abort_supervisor
{
AbortSupervisor::AbortSupervisor()
  : DsProcess(), d_ptr_(std::unique_ptr<AbortSupervisorPrivate>(new AbortSupervisorPrivate))
{
}
AbortSupervisor::AbortSupervisor(int argc, char** argv, const std::string& name)
  : DsProcess(argc, argv, name), d_ptr_(std::unique_ptr<AbortSupervisorPrivate>(new AbortSupervisorPrivate))
{
}

AbortSupervisor::~AbortSupervisor() = default;

void AbortSupervisor::setupPublishers()
{
  DsProcess::setupPublishers();
  DS_D(AbortSupervisor);

  d->msg_ = ds_core_msgs::Abort{};
  d->msg_.enable = true;
  d->msg_.ttl = 60;

  d->abort_pub_ = nodeHandle().advertise<ds_core_msgs::Abort>("abort", 1, true);
  d->countdown_pub_ = nodeHandle().advertise<ds_core_msgs::CountdownMonitor>("process_countdown", 1, true);
}

void AbortSupervisor::setupSubscriptions()
{
  DsProcess::setupSubscriptions();

  DS_D(AbortSupervisor);
  d->critical_process_sub_ = nodeHandle().subscribe("critical_process", 10, &AbortSupervisor::updateTtl, this);
}

void AbortSupervisor::setupParameters()
{
  DsProcess::setupParameters();

  DS_D(AbortSupervisor);
  d->aborting_ = false;

  ros::Time::init();
  scanCriticalProcesses();
}

void AbortSupervisor::setupServices()
{
  DsProcess::setupServices();
  DS_D(AbortSupervisor);

  d->abort_cmd_ = nodeHandle().advertiseService<ds_hotel_msgs::AbortCmd::Request, ds_hotel_msgs::AbortCmd::Response>(
      "abort_cmd", boost::bind(&AbortSupervisor::_abort_cmd, this, _1, _2));

  d->stop_ticking_shortman_cmd_ =
      nodeHandle().advertiseService<ds_core_msgs::VoidCmd::Request, ds_core_msgs::VoidCmd::Response>(
          "stop_ticking_shortman", boost::bind(&AbortSupervisor::_timer_off, this, _1, _2));

  d->rescan_params_cmd_ = nodeHandle().advertiseService<ds_base::BoolCommand::Request, ds_base::BoolCommand::Response>(
      "rescan_cmd", boost::bind(&AbortSupervisor::_rescan_params_cmd, this, _1, _2));
}

void AbortSupervisor::setupTimers()
{
  DsProcess::setupTimers();

  DS_D(AbortSupervisor);

  auto callback = [](AbortSupervisor* base, const ros::TimerEvent&) { base->publishAbortMessage(); };

  auto callback_cd = [](AbortSupervisor* base, const ros::TimerEvent&) { base->publishCountdownMessage(); };

  auto wrapped = boost::bind<void>(callback, this, _1);
  auto wrapped_cd = boost::bind<void>(callback_cd, this, _1);

  d->abort_refresh_timer_ = nodeHandle().createTimer(ros::Duration(10), wrapped);
  d->abort_refresh_timer_.start();

  d->countdown_refresh_timer_ = nodeHandle().createTimer(ros::Duration(1), wrapped_cd);
  d->countdown_refresh_timer_.start();
}

void AbortSupervisor::scanCriticalProcesses()
{
  DS_D(AbortSupervisor);

  // Stop countdown refresh timer
  d->countdown_refresh_timer_.stop();

  ros::Time now = ros::Time::now();
  //ROS_ERROR_STREAM("Now: " << now);

  // Clear the critical process map
  d->monitor_expirations_.clear();

  // Get the list of ALL parameter names on the parameter server
  std::vector<std::string> keys;
  bool result = ros::param::getParamNames(keys);
  if (result == true)
  {
    // Iterate through the keys and count how many "crtical" parameters
    // are set to true: this will be the number of processes that the abort
    // supervisor will be expecting to hear a ttl broadcast from
    for (auto key : keys)
    {
      // ROS_ERROR_STREAM(key);
      std::vector<std::string> splitkey;
      boost::algorithm::split(splitkey, key, boost::is_any_of("/"));
      if (splitkey.size() > 0)
      {
        if (splitkey[splitkey.size() - 1] == "critical")
        {
          std::string nodename;
          ROS_INFO_STREAM("Found critical node: " << splitkey[splitkey.size() - 1]);
          for (int i = 0; i < (splitkey.size() - 1); ++i)
          {
            if (i != 0)
            {
              nodename += "/";
            }
            nodename += splitkey[i];
          }
          ROS_INFO_STREAM("Critical node name: " << nodename);
          // Start a countdown of 60 seconds for this critical node
          // If we don't hear from the node within this time
          // Then the abort will be called. The countdown is reset
          // on receipt of a critical process ttl topic
          // By default set expirations 60s in the future (ttl 60)
          d->monitor_expirations_[nodename] = now + ros::Duration(60);
        }
      }
    }
    d->n_critical_processes_ = d->monitor_expirations_.size();
    d->cm_.nodes.resize(d->n_critical_processes_);
    int i = 0;
    for (std::pair<std::string, ros::Time> element : d->monitor_expirations_)
    {
      d->cm_.nodes[i].nodename = element.first;
      d->cm_.nodes[i].countdown = 60.0;
      ++i;
    }
  }

  // Start countdown refresh timer
  d->countdown_refresh_timer_.start();
}

void AbortSupervisor::publishAbortMessage()
{
  DS_D(AbortSupervisor);
  d->msg_.stamp = ros::Time::now();
  d->abort_pub_.publish(d->msg_);
}

void AbortSupervisor::stopAbortRefreshTimer()
{
  DS_D(AbortSupervisor);
  d->abort_refresh_timer_.stop();
}

void AbortSupervisor::publishCountdownMessage()
{
  DS_D(AbortSupervisor);

  // Do the countdown math and if necessary abort
  int i = 0;
  ros::Time now = ros::Time::now();
  // ROS_ERROR_STREAM("Now: " << now);
  for (std::pair<std::string, ros::Time> element : d->monitor_expirations_)
  {
    d->cm_.nodes[i].nodename = element.first;
    d->cm_.nodes[i].countdown = (element.second - now).toSec();
    ++i;
  }

  if (d->monitor_expirations_.size() > 0)
    d->countdown_pub_.publish(d->cm_);

  for (int i = 0; i < d->cm_.nodes.size(); ++i)
  {
    if (d->cm_.nodes[i].countdown < 0)
    {
      d->msg_.abort = true;
      d->msg_.enable = true;
      _abort();
      return;
    }
  }
}

void AbortSupervisor::updateTtl(const ds_core_msgs::CriticalProcess& msg)
{
  DS_D(AbortSupervisor);

  // Update ttl in the map if the key exists in the map
  if (d->monitor_expirations_.find(msg.nodename) != std::end(d->monitor_expirations_))
  {
    d->monitor_expirations_[msg.nodename] = ros::Time::now() + ros::Duration(msg.ttl);
  }
}

bool AbortSupervisor::_abort_cmd(ds_hotel_msgs::AbortCmd::Request req, ds_hotel_msgs::AbortCmd::Response resp)
{
  DS_D(AbortSupervisor);

  switch (req.command)
  {
    case ds_hotel_msgs::AbortCmd::Request::ABORT_CMD_ABORT:
      d->msg_.abort = true;
      break;
    case ds_hotel_msgs::AbortCmd::Request::ABORT_CMD_DONTABORT:
      d->msg_.abort = false;
      break;
    case ds_hotel_msgs::AbortCmd::Request::ABORT_CMD_ENABLE:
      d->msg_.enable = true;
      break;
    case ds_hotel_msgs::AbortCmd::Request::ABORT_CMD_DISABLE:
      d->msg_.enable = false;
      break;
    default:
      ROS_ERROR_STREAM("Unrecognized abort command: " << req.command);
      return false;
  }
  _abort();
  return true;
}

bool AbortSupervisor::_timer_off(ds_core_msgs::VoidCmd::Request req, ds_core_msgs::VoidCmd::Response resp)
{
  DS_D(AbortSupervisor);

  ROS_ERROR_STREAM("Stop ticking shortman command!");
  //    d->abort_refresh_timer_.stop();

  d->ticking_shortman_ = false;
  //    auto stop_msg = ds_core_msgs::Abort{};
  //    stop_msg.stamp = ros::Time::now();
  //    stop_msg.abort = d->msg_.abort;
  //    stop_msg.enable = d->msg_.enable;
  d->msg_.ttl = -1;
  d->abort_pub_.publish(d->msg_);

  resp.success = true;
  resp.msg = "Stopped the abort_supervisor short deadman";
  return true;
}

bool AbortSupervisor::_rescan_params_cmd(ds_base::BoolCommand::Request req, ds_base::BoolCommand::Response resp)
{
  DS_D(AbortSupervisor);

  // Rescan parameter server for critical processes if the command is true
  if (req.command)
  {
    scanCriticalProcesses();
  }

  return true;
}

bool AbortSupervisor::_abort(void)
{
  DS_D(AbortSupervisor);

  publishAbortMessage();
  if ((d->msg_.enable) && (d->msg_.abort) && (!d->aborting_))
  {
    d->aborting_ = true;
    executeAbort();
  }
  return true;
}
}
