/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 1/17/18.
//

#ifndef PROJECT_LAMBDAPOWERSUPPLY_PRIVATE_H
#define PROJECT_LAMBDAPOWERSUPPLY_PRIVATE_H

#include <ds_lambda_ps/lambda_power_supply.h>
#include <ds_core_msgs/Status.h>
#include <ds_core_msgs/RawData.h>
#include <ds_hotel_msgs/PowerSupply.h>
#include <ds_hotel_msgs/PowerSupplyCommand.h>

#include <sstream>
#include <iomanip>

namespace ds_components
{
struct LambdaPowerSupplyPrivate
{
  LambdaPowerSupplyPrivate(LambdaPowerSupply* base) : address_command_(ds_asio::IoCommand(1.0)), q_ptr_(base)
  {
  }
  virtual ~LambdaPowerSupplyPrivate() = default;
  DS_DISABLE_COPY(LambdaPowerSupplyPrivate);
  DS_DECLARE_PUBLIC(LambdaPowerSupply);

  const static std::string PARSE_REGEX_STR;

  bool _query_recv(const ds_core_msgs::RawData& bytes)
  {
    last_message_timestamp_ = bytes.header.stamp;
    std::string msg(reinterpret_cast<const char*>(bytes.data.data()), bytes.data.size() - 1);

    // ROS_ERROR_STREAM("_data_recv callback: \"" <<msg <<"\"");

    // temp values
    uint32_t sr = 0, fr = 0;
    boost::regex parseRegex{ PARSE_REGEX_STR };
    boost::smatch results;
    if (boost::regex_search(msg, results, parseRegex))
    {
      // Parse the actual message
      ds_hotel_msgs::PowerSupply ret;
      ret.meas_volts = strtof(results[1].str().c_str(), NULL);
      ret.prog_volts = strtof(results[2].str().c_str(), NULL);
      ret.meas_amps = strtof(results[3].str().c_str(), NULL);
      ret.prog_amps = strtof(results[4].str().c_str(), NULL);
      sr = strtol(results[5].str().c_str(), NULL, 16);
      fr = strtol(results[6].str().c_str(), NULL, 16);

      // Fill in the header, copying the data
      ret.ds_header = bytes.ds_header;
      ret.header.stamp = ret.ds_header.io_time;
      std::copy(std::begin(uuid_.data), std::end(uuid_.data), std::begin(ret.ds_header.source_uuid));

      // Parse the fault message
      ret.status_msg = "";
      if (fr & 0x02)
      {
        ret.status_msg += " AC_Fail";
        ROS_ERROR_STREAM("AC FAIL");
      }
      if (fr & 0x04)
      {
        ret.status_msg += " Over_Temp";
        ROS_ERROR_STREAM("OVER TEMP");
      }
      if (fr & 0x08)
      {
        ret.status_msg += " Foldback";
        ROS_ERROR_STREAM("FOLD BACK (current limit hit)");
      }
      if (fr & 0x10)
      {
        ret.status_msg += " Over_Voltage";
        ROS_ERROR_STREAM("OVER VOLTAGE");
      }
      if (fr & 0x20)
      {
        ret.status_msg += " Shut_Off";
        ROS_ERROR_STREAM("SHUT OFF");
      }
      if (fr & 0x40)
      {
        ret.status_msg += " Output_Off";
        // ROS_ERROR_STREAM("OUTPUT OFF");
      }
      if (fr & 0x80)
      {
        ret.status_msg += " Enable";
        ROS_ERROR_STREAM("ENABLE");
      }

      ret.output_enable = (sr & 0x03);               // constant current OR constant voltage
      ret.status_good = (sr & 0x03) && (sr & 0x04);  // constant current OR constant voltage AND no fault
      ret.frontpanel_locked = !(sr & 0x80);
      ret.fault = (sr & 0x08);
      ret.constant_voltage = (sr & 0x01);
      ret.constant_current = (sr & 0x02);

      ret.idnum = idnum_;

      last_message_ = ret;
      ds_ps_publisher.publish(ret);

      return true; // accept this string as valid
    }
    else
    {
      std::string mutableMsg(msg);
      DS_Q(LambdaPowerSupply);
      ROS_ERROR_STREAM("LambdaPowerSupply::" << q->descriptiveName() << " REGEX MISMATCH! \"" << msg << "\"");

      return false;
    }
  }

  bool _service_req(const ds_hotel_msgs::PowerSupplyCommand::Request& req,
                    ds_hotel_msgs::PowerSupplyCommand::Response& resp)
  {
    ROS_ERROR_STREAM("LAMBDA SERVICE RECEIVED v:" << req.prog_volts << " c:" << req.prog_amps);

    // (possibly) turn off the output
    if (last_message_.output_enable && !req.output_enable)
    {
      send_command("OUT 0\r");
    }

    // set voltage
    if (req.prog_volts != req.POWERSUPPLY_NODATA && last_message_.prog_volts != req.prog_volts &&
        req.prog_volts <= max_voltage_limit_ && req.prog_volts >= min_voltage_limit_)
    {
      std::stringstream cmd;
      cmd << "PV " << std::fixed << std::setprecision(3) << req.prog_volts << "\r";
      send_command(cmd.str());
    }

    // set voltage
    if (req.prog_amps != req.POWERSUPPLY_NODATA && last_message_.prog_amps != req.prog_amps &&
        req.prog_amps <= max_current_limit_ && req.prog_amps >= min_current_limit_)
    {
      std::stringstream cmd;
      cmd << "PC " << std::fixed << std::setprecision(3) << req.prog_amps << "\r";
      send_command(cmd.str());
    }

    // (possibly) turn on the output
    if (!last_message_.output_enable && req.output_enable)
    {
      send_command("OUT 1\r");
    }

    // Set the user lockout
    if (last_message_.frontpanel_locked != req.frontpanel_locked)
    {
      if (req.frontpanel_locked)
      {
        send_command("RMT LLO\r");
      }
      else
      {
        send_command("RMT LOC\r");
      }
    }

    resp.success = 1;
    return true;
  }

  bool _command_result_recv(const std::string& cmdstr, const ds_core_msgs::RawData& bytes)
  {
    std::string msg(reinterpret_cast<const char*>(bytes.data.data()), bytes.data.size() - 1);
    if (msg == "OK")
    {
      return true;
    }
    std::string cmd = cmdstr.substr(0, cmdstr.size() - 1);
    std::string desc = "Unknown";
    if (msg == "E01")
    {
      desc = "Requested voltage is above hardware voltage range (> 105% of supply or 95% of OVP)";
    }
    else if (msg == "E02")
    {
      desc = "Requested voltage is below hardware limit (below UVL setting)";
      // There is no E03
    }
    else if (msg == "E04")
    {
      desc = "OVP is programmed below acceptable range";
      // Also no E05
    }
    else if (msg == "E06")
    {
      desc = "UVL is programmed above programmed output voltage";
    }
    else if (msg == "E07")
    {
      desc = "Attempted to turn output ON during a fault shutdown";
    }
    else if (msg == "C01")
    {
      desc = "Illegal command or query";
    }
    else if (msg == "C02")
    {
      desc = "Missing parameter";
    }
    else if (msg == "C03")
    {
      desc = "Illegal parameter";
    }
    else if (msg == "C04")
    {
      desc = "Checksum error";
    }
    else if (msg == "C05")
    {
      desc = "Setting out of range";
    }
    ROS_ERROR_STREAM("Got unexpected Lambda Power Supply Result for command: \"" << cmd << "\" : \"" << msg
                                                                                 << "\" -> \"" << desc << "\"");
    return true;
  }

  void send_command(const std::string& cmdstr)
  {
    ROS_ERROR_STREAM("Lambda cmd sent:" << cmdstr);
    iosm->addPreemptCommand(address_command_);
    iosm->addPreemptCommand(ds_asio::IoCommand(
        cmdstr, 0.250, true, boost::bind(&LambdaPowerSupplyPrivate::_command_result_recv, this, cmdstr, _1)));
    // ROS_ERROR_STREAM("Queued command: " <<cmdstr);
  }

  /// @brief Power supply publisher
  ros::Publisher ds_ps_publisher;

  /// @brief Command service handler
  ros::ServiceServer cmd_serv_;

  /// @brief I/O state machine
  boost::shared_ptr<ds_asio::IoSM> iosm;

  /// @brief The Lambda power supply line needs an address command before every real command
  ds_asio::IoCommand address_command_;

  /// @brief The power supply UUID
  boost::uuids::uuid uuid_;

  /// @brief The timestamp of the last message from the I/O state machine
  ros::Time last_message_timestamp_;

  ds_hotel_msgs::PowerSupply last_message_;

  /// @brief The time duration between valid messages to consider bad
  ros::Duration message_timeout_;

  /// @brief Address of the power supply on the bus
  int address_;

  /// @brief ID number for debugging purposes (Human usable, not necessarily same as address_
  int idnum_;

  /// @brief Hard minimum voltage limit imposed by parameter server (for safety!)
  float min_voltage_limit_;

  /// @brief Hard maximum voltage limit imposed by parameter server (for safety!)
  float max_voltage_limit_;

  /// @brief Hard minimum current limit imposed by parameter server (for safety!)
  float min_current_limit_;

  /// @brief Hard maximum current limit imposed by parameter server (for safety!)
  float max_current_limit_;

  LambdaPowerSupply* q_ptr_;
};
}

#endif  // PROJECT_LAMBDAPOWERSUPPLY_PRIVATE_H
