/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#include "ds_timegen/ds_timegen.h"

namespace ds_timegen
{
DsTimegen::DsTimegen() : DsProcess()
{
  timeThread_.reset(new boost::thread(boost::bind(&DsTimegen::timegenThread, this)));
}

DsTimegen::DsTimegen(int argc, char* argv[], const std::string& name) : DsProcess(argc, argv, name)
{
  timeThread_.reset(new boost::thread(boost::bind(&DsTimegen::timegenThread, this)));
}

DsTimegen::~DsTimegen() = default;

void DsTimegen::setupParameters() {
  ds_base::DsProcess::setupParameters();
  if (!nh.getParam(ros::this_node::getName() + "/" + "connections", conn_names)) {
    ROS_FATAL_STREAM("MUST specify connection names!");
    ROS_BREAK();
  }
}

int DsTimegen::hostCheckSum(char* str)
{
  int length;  // integer variable for length of string
  int i;       // integer indexing variable
  int csum;    // integer variable for check sum computation

  // initialize the check sum to zero
  csum = 0;

  // determine the length of the string
  length = strlen(str);

  // compute the checksum as the exclusive-or (XOR) of the
  // characters in the string
  // The C language symbol for the XOR operator is "^"
  for (i = 0; i < length; i++)
  {
    csum = csum ^ str[i];
  }

  // return the lower 8 bits of the checksum
  return (0xFF & csum);
}

struct timespec DsTimegen::tsAdd(struct timespec time1, struct timespec time2)
{
  struct timespec result;
  result.tv_sec = time1.tv_sec + time2.tv_sec;
  result.tv_nsec = time1.tv_nsec + time2.tv_nsec;
  if (result.tv_nsec >= 1000000000L)
  { /* Carry? */
    result.tv_sec++;
    result.tv_nsec = result.tv_nsec - 1000000000L;
  }
  return (result);
}

void DsTimegen::onIncomingData(ds_core_msgs::RawData in)
{
}

void DsTimegen::timegenThread(void)
{
  // Structs for the timer
  struct timespec timerTime;
  struct timespec timerPeriod;

  // Structs for the actual time we're sending
  struct timespec current_time;

  // serial output buffer
  char zdastring[256];
  int zdalen = 0;

  /*
    $GPZDA,hhmmss.ss,xx,xx,xxxx,xx,xx
    hhmmss.ss = UTC
    xx = Day, 01 to 31
    xx = Month, 01 to 12
    xxxx = Year
    xx = Local zone description, 00 to +/- 13 hours
    xx = Local zone minutes description (same sign as hours)
  */

  // wait until the top of the second
  timerPeriod.tv_sec = 1;
  timerPeriod.tv_nsec = 000000000;
  clock_gettime(CLOCK_REALTIME, &timerTime);
  timerTime.tv_sec += 2;
  timerTime.tv_nsec = 000000000;

  while (1)
  {
    // wait for the top of the second
    timerTime = tsAdd(timerTime, timerPeriod);
    clock_nanosleep(CLOCK_REALTIME, TIMER_ABSTIME, &timerTime, NULL);

    // grab the current time, convert to tm
    clock_gettime(CLOCK_REALTIME, &current_time);
    struct tm* tm = gmtime(&(current_time.tv_sec));

    // clear our output string buffer
    memset(zdastring, 0, sizeof(zdastring));
    zdalen = 0;

    // build the ZDA string
    zdalen += sprintf(&zdastring[zdalen], "$GPZDA,%02d%02d%02d.00,%02d,%02d,%04d,00,00", tm->tm_hour, tm->tm_min,
                      tm->tm_sec, tm->tm_mday, tm->tm_mon + 1, tm->tm_year + 1900);
    zdalen += sprintf(&zdastring[zdalen], "*%02X\r\n", hostCheckSum(&zdastring[1]));
    // write the output
    sendTimeCallback(zdastring);
  }
}

void DsTimegen::sendTimeCallback(std::string in)
{
  ROS_INFO_STREAM("Time: " << in);
  for (std::pair<std::string, boost::shared_ptr<ds_asio::DsConnection> > device : conn_)
    device.second->send(in);
}

void DsTimegen::setupConnections(void)
{
  ds_base::DsProcess::setupConnections();

  for (size_t i=0;i<conn_names.size();i++) {
    conn_name = conn_names[i];
    conn_[conn_name] = addConnection(conn_name, boost::bind(&DsTimegen::onIncomingData, this, _1));
    ROS_WARN_STREAM("Loading connection: "<<conn_name);
  }
  //conn_["edgetech"] = addConnection("edgetech", boost::bind(&DsTimegen::onIncomingData, this, _1));
  //conn_["reson"] = addConnection("reson", boost::bind(&DsTimegen::onIncomingData, this, _1));
  //conn_["phins"] = addConnection("phins", boost::bind(&DsTimegen::onIncomingData, this, _1));
}
}
