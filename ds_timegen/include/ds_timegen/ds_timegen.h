/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#ifndef DS_TIMEGEN_H
#define DS_TIMEGEN_H

#include <ds_base/ds_process.h>
#include <ds_base/util.h>
#include <ds_asio/ds_connection.h>
#include <boost/thread.hpp>

namespace ds_timegen
{
class DsTimegen : public ds_base::DsProcess
{
public:
  DsTimegen();
  DsTimegen(int argc, char* argv[], const std::string& name);
  ~DsTimegen() override;
  struct timespec tsAdd(struct timespec time1, struct timespec time2);
  int hostCheckSum(char* str);
  void onIncomingData(ds_core_msgs::RawData in);
  void timegenThread(void);
  void sendTimeCallback(std::string in);

protected:
  void setupParameters() override;
  void setupConnections() override;

  ros::NodeHandle nh;
  std::string conn_name;
  std::vector<std::string> conn_names;

private:
  std::unordered_map<std::string, boost::shared_ptr<ds_asio::DsConnection> > conn_;
  std::unique_ptr<boost::thread> timeThread_;
};
}
#endif  // DS_TIMEGEN_H
