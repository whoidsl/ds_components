/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#ifndef DS_ELMO_ELMOMOTORCONTROLLER_H
#define DS_ELMO_ELMOMOTORCONTROLLER_H

#include "ds_base/ds_process.h"

namespace ds_elmo
{
struct ElmoMotorControllerPrivate;

/// @brief Base class for Elmo motor controllers
///
/// # Parameters
///
/// In addition to the parameters used by `DsProcess`, `ElmoMotorController` looks for the
/// following:
///
///   - `~init_commands`:  A list of commands to send to the controller at startup
///   - `~poll_commands`:  A list of commands to regularly poll the controller with
///
/// # Connections
///
/// The elmo controller expects connection information under a private `~elmo` parameter
/// namespace.
class ElmoMotorController : public ds_base::DsProcess
{
  DS_DECLARE_PRIVATE(ElmoMotorController)

public:
  using TimeStampedParam = std::pair<ros::Time, double>;
  using TimeStampedParamMap = std::unordered_map<std::string, TimeStampedParam>;
  using PollCommand = std::pair<uint64_t, ds_asio::IoCommand>;

  ElmoMotorController();
  ElmoMotorController(int argc, char* argv[], const std::string& name);
  ~ElmoMotorController() override;
  DS_DISABLE_COPY(ElmoMotorController);

  ///@brief Get access to the underlying elmo state machine
  ///
  /// The ElmoMotorController retains ownership of the state machine.
  ///
  /// \return
  ds_asio::IoSM* elmoStateMachine();

  /// @brief Get the last reported value for the named parameter.
  ///
  /// NOTE:  This does not query the motorcontroller, just returns
  ///        the last observed value.
  ///
  ///
  /// An invalid timestamp is returned if the provided name has not been recorded.
  /// \param name
  /// \return
  TimeStampedParam parameter(const std::string& name);

  const TimeStampedParamMap& parameters();

  /// @brief Add a one-shot prempt command
  ///
  /// \param command
  void addPreemptCommand(ds_asio::IoCommand command);

  /// @brief Add a recurring command
  ///
  /// \param command
  /// \return
  uint64_t addRegularCommand(ds_asio::IoCommand command);

  /// @brief Replace a recurring command.
  ///
  /// \param id
  /// \param command
  bool replaceRegularCommand(uint64_t id, ds_asio::IoCommand command);
  const std::list<PollCommand>& polledCommands();

  void setup() override;

protected:
  void setupConnections() override;

  /// @brief An additional setup directive for Elmo controllers.
  ///
  /// This method is called within setup(), after all of the normal
  /// DsProcess-based initialization occours.
  ///
  /// The default behavior is to populate the internal state machine
  /// with a set of initialization commands and a set of poll commands.
  /// These are read from the parameter server variables
  /// `~init_commands` and `~poll_commands` respectively.
  ///
  /// Additionally, the controller is ALWAYS set with disabled echo (`EO=0`)
  /// and disabled motor output (`MO=0`) as the first two initialization
  /// commands set regardless of what is read from the parameter server.
  virtual void setupCommands();

private:
  std::unique_ptr<ElmoMotorControllerPrivate> d_ptr_;
};
}
#endif  // DS_ELMO_ELMOMOTORCONTROLLER_H
