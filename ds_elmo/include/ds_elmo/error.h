/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#ifndef DS_ELMO_ERROR_H
#define DS_ELMO_ERROR_H

#include <ostream>
namespace ds_elmo
{
namespace error_code
{
enum ErrorCode
{
  BAD_COMMAND = 2,                    //!< The interpreter has not understood the command.
  BAD_INDEX = 3,                      //!< Attempt to access a vectored variable out of its boundaries.
  BAD_CHARACTER = 5,                  //!< An unrecognized character has been found
                                      //!< where a command was expected.
  PROGRAM_NOT_RUNNING = 6,            //!< Program is not running. This command requires a running program.
  MODE_UNABLE_TO_START = 7,           //!< Mode cannot be started - bad initialization data.
  WRITE_ERROR = 11,                   //!< Cannot write to flash memory.
  COMMAND_UNAVAILABLE = 12,           //!< Command not available in this unit mode.
  CANNOT_RESET_COMMS = 13,            //!< UART is busy and cannot reset parameters
  EMPTY_ASSIGN = 18,                  //!< Missing value for parameter assignment
  BAD_COMMAND_FORMAT = 19,            //!< Bad command format.
  OPERAND_OUT_OF_RANGE = 21,          //!< Operand out of range.
  ZERO_DIVISION = 22,                 //!< Zero division.
  UNABLE_TO_ASSIGN = 23,              //!< Command cannot be assigned. (command does not take a value)
  BAD_OPERATOR = 24,                  //!< Bad operator.
  CMD_NOT_VALID_WHEN_MOVING = 25,     //!< Command not valid while moving.
  MOTION_MODE_NOT_VALID = 26,         //!< Motion mode not valid.
  OUT_OF_LIMIT_RANGE = 28,            //!< Out of limit range.
  NO_PROGRAM_TO_CONTINUE = 30,        //!< No program to continue.
  COMMS_OVERRUN = 32,                 //!< Communication error
  DUPLICATE_HALL_SENSOR = 37,         //!< Two or more Hall sensors are defined for the same location.
  MOTION_START_TIMEOUT = 39,          //!< Motion start specified for the past.
  COMMAND_NOT_SUPPORTED = 41,         //!< Command is not supported by this product
  NO_SUCH_LABEL = 42,                 //!< No such label.
  CAN_STATE_MACHINE_FAULT = 43,       //!< CAN state machine fault (object 0x6040 on
  SUBROUTINE_ERROR = 45,              //!< Returned error from subroutine.
  HOMING_MODE_STOP = 46,              //!< May not use multi-capture homing mode with stop event.
  PROGRAM_DOES_NO_EXIST = 47,         //!< User program does not exist. XQ or XC
  STACK_OVERFLOW = 50,                //!< Stack overflow.
  ONLY_FOR_CURRENT = 53,              //!< Command is applicable only in torque modes UM=1 or TC=2
  BAD_DATABASE = 54,                  //!< Bad database.
  BAD_CONTEXT = 55,                   //!< Bad context
  ADV_COMMAND_NOT_SUPPORTED = 56,     //!< The product grade does not support this command.
  MOTOR_MUST_BE_OFF = 57,             //!< Motor must be off to use this command.
  MOTOR_MUST_BE_ON = 58,              //!< Motor must be on to use this command.
  BAD_UNIT_MODE = 60,                 //!< Bad unit mode.
  DATABASE_RESET = 61,                //!< Database reset to factory defaults.
  CANNOT_SET_INDEX = 64,              //!< Cannot set the index of an active table. When the ECAM table is active,
  DISABLED_BY_SW = 65,                //!< Disabled by SW.
  DRIVE_NOT_READY = 66,               //!< Drive not ready.
  RECORDER_BUSY = 67,                 //!< Recorder is busy.
  RECORDER_ERROR = 69,                //!< Recorder usage error.
  RECORDER_DATA_INVALID = 70,         //!< Recorder data invalid.
  HOMING_BUSY = 71,                   //!< Homing is busy.
  MUST_BE_EVEN = 72,                  //!< Must be even.
  SET_POSITION = 73,                  //!< Please set position.
  BUFFER_TO_LARGE = 77,               //!< Buffer too large.
  PROGRAM_OUT_OF_RANGE = 78,          //!< Out of program range.
  ECAM_DATA_INCONSISTENT = 80,        //!< ECAM data inconsistent.
  PROGRAM_RUNNING = 82,               //!< Program is running.
  COMMAND_NOT_FOR_PROGRAM = 83,       //!< CMD not for program.
  SYSTEM_NOT_IN_POINT_TO_POINT = 84,  //!< The system is not in point to point mode. A PR (position relative) cannot
  CAN_STATE_MACHINE_NOT_READY = 90,   //!< CAN state machine is not ready (object
  BAD_INITIATION_VALUE = 93,          //!< There is a wrong initiation value for this
  TOO_BIG_FOR_MODULO = 95,            //!< Too large for modulo setting. The modulo range is inconsistent
  USER_PROGRAM_TIMEOUT = 96,          //!< User program time out.
  RS232_RX_OVERFLOW = 97,             //!< RS232 receive buffer overflow.
  AUX_FEEDBACK = 99,                  //!< The auxiliary feedback entry does not
  PWM_NOT_SUPORTED = 100,             //!< The requested PWM value is not supported
  POSITION_READ_ABORT = 101,          //!< Abortive attempt to read a position value
  SPEED_KP_RANGE = 105,               //!< Speed loop KP out of range.
  POSITION_KP__RANGE = 106,           //!< Position loop KP out of range.
  KV_N_INVALID = 111,                 //!< KV[N] vector is invalid.
  KV_SCHEDULE_OFF = 112,              //!< KV[N] defines scheduled block but
  EXP_TASK_QUEUE_FULL = 113,          //!< Exp task queue is full.
  EXP_TASK_QUEUE_EMPTY = 114,         //!< Exp task queue is empty.
  EXP_OUTPUT_QUEUE_FULL = 115,        //!< Exp output queue is full
  EXP_OUTPUT_QUEUE_EMPTY = 116,       //!< Exp output queue is empty.
  BAD_KV_SETTING = 117,               //!< Bad KV setting for sensor filter.
  BAD_KV_VECTOR = 118,                //!< Bad KV vector This can happen when KV
  BAD_ANALOG_SENSOR = 119,            //!< Bad Analog sensor Filter When the filter KV, set for analog
  BAD_NUM_ANALOG_BLOCKS = 120,        //!< Bad number of blocks for Analog sensor
  ANALOG_SENSOR_NOT_READY = 121,      //!< Analog sensor is not ready When the initiation procedure of
  MODULO_NEGATIVE = 127,              //!< Modulo range must be positive.
  NOT_AN_ARRAY = 129,                 //!< Variable is not an array.
  DOES_NOT_EXIST = 130,               //!< Variable name does not exist.
  CANNOT_RECORD_LOCAL = 131,          //!< Cannot record local variables.
  NOT_AN_ARRAY_INTERNAL = 132,        //!< Variable is not an array.
  MUSMATCH_NUM_USER_SYSTEM = 133,     //!< Mismatched number of user/system
  CANNOT_RUN_LABEL = 134,
  ALREADY_COMPILED = 137,             //!< Program already compiled.
  TOO_MANY_BREAKPOINTS = 139,         //!< The number of breakpoints exceeds the
  FAILED_TO_MODIFY_BREAKPOINT = 140,  //!< An attempt to set/clear breakpoint at the
  BOOT_ID_NON_EMPTY = 141,            //!< Boot identity parameters section is not
  INVALID_CHECKSUM = 142,             //!< Checksum of data is not correct.
  MISSING_BOOT_ID = 143,              //!< Missing boot identity parameters.
  NUMERIC_STACK_UNDERFLOW = 144,      //!< Numeric stack underflow.
  NUMERIC_STACK_OVERLFLOW = 145,      //!< Numeric stack overflow.
  EXPR_STACK_OVERFLOW = 146,          //!< Expression stack overflow.
  COMMAND_WITHIN_MATH = 147,          //!< Executable command within math
  EMPTY_EXPRESSION = 148,             //!< Nothing in the expression.
  UNEXPECTED_EOS = 149,               //!< Unexpected sentence termination.
  EXPRESSION_TO_LONG = 150,           //!< Sentence terminator not found.
  MISMATCHED_PARENTHESES = 151,       //!< Parentheses mismatch.
  BAD_OPERAND_TYPE = 152,             //!< Bad operand type.
  ADDR_OUT_OF_MEM_RANGE = 154,        //!< Address is out of data memory segment.
  ADDR_OUT_OF_STACK_RANGE = 155,      //!< Beyond stack range.
  BAD_OPCODE = 156,                   //!< Bad opcode.
  NO_PROGRAM_STACK = 157,             //!< No available program stack.
  ADDR_OUT_OF_FLASH_RANGE = 158,      //!< Out of flash memory range.
  FLASH_VERIFY_ERROR = 159,           //!< Flash verify error.
  PROGRAM_TERMINATED = 160,           //!< Program aborted by another threat.
  PROGRAM_NOT_HALTED = 161,           //!< Program is not halted.
  BAD_NUMBER = 162,                   //!< Badly formatted number.
  EC_COMMAND = 164,                   //!< EC command (not an error).
  SERIAL_FLASH_BUSY = 165,            //!< An attempt to access serial flash while busy. Contact Technical Support.
  OUT_OF_MODULO_RANGE = 166,          //!< Out of modulo range. XM[1]=-1000, XM[2]=1000 and
  INFINITE_LOOP = 167,                //!< Infinite loop in for loop - zero step k=1:0:10; causes this error.
  SPEED_TO_LARGE = 168,               //!< Speed too large to start motor.
  UNKNOWN = 255,                      //!< Unknown error (only used when converting from number)
};

std::ostream& operator<<(std::ostream& os, const int err);
std::string to_string(const int err);
}
}

#endif  // DS_ELMO_ERROR_H
