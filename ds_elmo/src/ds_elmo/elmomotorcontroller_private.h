/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
#ifndef DS_ELMO_ELMOMOTORCONTROLLER_PRIVATE_H
#define DS_ELMO_ELMOMOTORCONTROLLER_PRIVATE_H

#include "ds_elmo/elmomotorcontroller.h"
#include "ds_asio/ds_iosm.h"
#include "ds_core_msgs/RawData.h"

namespace ds_elmo
{
struct ElmoMotorControllerPrivate
{
  ElmoMotorControllerPrivate();
  ~ElmoMotorControllerPrivate();

  ds_asio::IoCommand wrap_command_for_logging(ds_asio::IoCommand command);

  bool elmo_command_callback(const std::string& command, ds_core_msgs::RawData msg,
                             ds_asio::IoCommand::ReadCallback user_callback = ds_asio::IoCommand::ReadCallback());

  bool has_started_;
  std::list<ds_asio::IoCommand> init_commands_;
  std::list<ElmoMotorController::PollCommand> poll_commands_;
  std::unordered_map<std::string, ElmoMotorController::TimeStampedParam> parameters_;  //!< Map of parameters reported
                                                                                       //! by the instrument
  boost::shared_ptr<ds_asio::DsConnection> elmo_;                                      //!< The Elmo I/O connection.
  boost::shared_ptr<ds_asio::IoSM> elmo_sm_;                                           //!< The Elmo state machine.
};
}
#endif  // DS_ELMO_ELMOMOTORCONTROLLER_PRIVATE_H
