/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/

#include "elmomotorcontroller_private.h"
#include "ds_elmo/error.h"

#include <sstream>
#include <iterator>
#include <limits>
#include <boost/algorithm/string.hpp>

namespace ds_elmo
{
ElmoMotorControllerPrivate::ElmoMotorControllerPrivate() : has_started_(false)
{
}

ElmoMotorControllerPrivate::~ElmoMotorControllerPrivate() = default;

ds_asio::IoCommand ElmoMotorControllerPrivate::wrap_command_for_logging(ds_asio::IoCommand command)
{
  auto cmd = command.getCommand();
  if (cmd.find('\r') == std::string::npos)
  {
    cmd.append("\r");
    command.setCommand(cmd);
  }

  auto orig_callback = command.getCallback();
  auto wrapper = boost::bind(&ElmoMotorControllerPrivate::elmo_command_callback, this, cmd, _1, orig_callback);
  command.setCallback(wrapper);
  return command;
}

bool ElmoMotorControllerPrivate::elmo_command_callback(const std::string& command, ds_core_msgs::RawData msg,
                                                       ds_asio::IoCommand::ReadCallback user_callback)
{
  bool rc;
  if (user_callback)
  {
    rc = user_callback(msg);
  }

  std::string response = std::string(std::begin(msg.data), std::end(msg.data));

  std::string _command(command);
  boost::replace_all(_command, "\r", "\\r");
  std::string _response(response);
  boost::replace_all(_response, "\r", "\\r");
  ROS_DEBUG_STREAM("Command: \"" << _command <<"\" Response: \"" <<_response <<"\"");

  // Look for error in return
  const auto error_it = std::find(std::begin(msg.data), std::end(msg.data), '?');
  if (error_it != std::end(msg.data))
  {
    const auto err = *std::prev(error_it);
    ROS_ERROR_STREAM("Error response to command '" << command << "'.  (" << static_cast<int>(err)
                                                   << "): " << error_code::to_string(err));
    return false;
  }

  std::string key;
  double value = 0.0;

  // all commands are sent using a carriage return to delimit between TX and RX
  const auto tx_end = response.find("\r");
  if (tx_end == std::string::npos) {
    ROS_ERROR_STREAM("Command \"" <<command <<"\" produced unreadable response \"" <<response <<"\"");
    false;
  }

  std::string recv_command = response.substr(0, tx_end); // strip trailing \r
  std::string recv_reply = response.substr(tx_end+1); // to end

  // ignore labelled commands
  if (recv_command.find("##") != std::string::npos)
  {
    ROS_DEBUG_STREAM("Ignoring labeled command: " << command);
    return true;
  }

  // Set commands do not return a value, so we'll recover the parameter value
  // from the command itself
  const auto set_pos = recv_command.find('=');
  std::string value_str;

  // handle commands differently from queries
  if (set_pos != std::string::npos) {
    key = recv_command.substr(0, set_pos);
    value_str = recv_command.substr(set_pos + 1);
  } else {
    // handle queries
    const auto rx_end = recv_reply.find(';');
    key = recv_command;
    value_str = recv_reply.substr(0,rx_end); // strip the trailing semicolon
  }

  // we now have a key/value to put in the dictionary.  Convert the value to a double
  try
  {
    value = std::stod(value_str);
  }
  catch (std::exception& e)
  {
    ROS_WARN_STREAM("Unable to convert '" << value_str << "' to double in command '" << recv_command << "'");
    return false;
  }
  ROS_DEBUG_STREAM("QUERY \"" <<key <<"\" gives \"" <<value_str <<"\" from command \"" <<_command <<"\"");

  // Update the parameter
  parameters_[key] = { msg.ds_header.io_time, value };

  // Now, check to see if this reply corresponds to the command we sent out while ignoring the trailing \r
  if (command.compare(0, command.size()-1, recv_command) == 0) {
    return true;
  }
  ROS_ERROR_STREAM("Elmo sync error: command \"" <<command.substr(0, command.size()-1) <<"\" saw reply \"" <<recv_command <<"\"");
  return false;
}
}