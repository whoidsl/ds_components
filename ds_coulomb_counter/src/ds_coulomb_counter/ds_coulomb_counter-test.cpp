/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/

#include "ds_coulomb_counter.h"
#include "ros/ros.h"
#include "ds_core_msgs/RawData.h"

#include <list>
#include <gtest/gtest.h>

namespace ds_components
{
class CoulombCounterTest : public ::testing::Test
{
public:
  // This method runs ONCE before a text fixture is run
  static void SetUpTestCase()
  {
    ros::Time::init();
  }
};

TEST_F(CoulombCounterTest, test_test)
{
  // Simple function to create a ds_sensors_msg::VectorMagneticField from arguments
  auto charge = [](float x, float y, float z, float w) {

    auto _msg = ds_hotel_msgs::Charge{};
    _msg.charge[0] = x;
    _msg.charge[1] = y;
    _msg.charge[2] = z;
    _msg.charge[3] = w;

    return _msg;
  };

  // Add new test cases here.  These should pass
  typedef std::pair<std::string, boost::array<double, 4>> TestPair;
  const auto test_datas = std::list<TestPair> { TestPair("+0.2836      +0.1940      +0.2217        +1.0947\n", {0.2836, 0.1940, 0.2217, 1.0947}),
                                                TestPair("+0.2740      -0.1856      -0.2074        -0.3577\n", {0.2740, -0.1856, -0.2074, -0.3577}) };

  // Loop through all provided cases
  for (const auto& test_data : test_datas)
  {
    auto test_str = test_data.first;
    auto test_expected = test_data.second;

    // set "should be" data as charge msg
    auto test_rtrn_msg = ds_hotel_msgs::Charge{};
    test_rtrn_msg.charge = test_expected;

    // Construct a ByteSequence message
    auto byte_msg = ds_core_msgs::RawData{};
    auto now = ros::Time::now();
    byte_msg.ds_header.io_time = now;
    byte_msg.data = std::vector<unsigned char>(std::begin(test_str), std::end(test_str));

    // set these objects to the parsed return for comparison
    auto ok = false;
    auto parsed_msg = ds_hotel_msgs::Charge{};
    std::tie(ok, parsed_msg) = CoulombCounter::parse_charge(byte_msg);

    // Should have succeeded
    EXPECT_TRUE(ok);

    // All fields should match.
    EXPECT_FLOAT_EQ(now.toSec(), parsed_msg.header.stamp.toSec());
    EXPECT_FLOAT_EQ(test_rtrn_msg.charge[0], parsed_msg.charge[0]);
    EXPECT_FLOAT_EQ(test_rtrn_msg.charge[1], parsed_msg.charge[1]);
    EXPECT_FLOAT_EQ(test_rtrn_msg.charge[2], parsed_msg.charge[2]);
    EXPECT_FLOAT_EQ(test_rtrn_msg.charge[3], parsed_msg.charge[3]);
  }
}
}

// wrapper used to execute all gtests
int main(int argc, char** argv)
{
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
