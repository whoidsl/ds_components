/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*
*/

#include "ros/ros.h"
#include "ds_coulomb_counter_private.h"
#include "ds_core_msgs/RawData.h"
#include "ds_hotel_msgs/Charge.h"

#include "ds_hotel_msgs/ChargeCmd.h"

#include <stdio.h>

namespace ds_components
{
CoulombCounter::CoulombCounter() : ds_base::SensorBase(), d_ptr_(new CoulombCounterPrivate)
{
}

CoulombCounter::CoulombCounter(int argc, char* argv[], const std::string& name)
  : SensorBase(argc, argv, name), d_ptr_(std::unique_ptr<CoulombCounterPrivate>(new CoulombCounterPrivate))
{
}

CoulombCounter::~CoulombCounter() = default;

void CoulombCounter::setupConnections()
{
  ds_base::DsProcess::setupConnections();

  // add connection to instrument and bind input to CoulombCounter::parseRecievedBytes()
  DS_D(CoulombCounter);
  board_connection = addConnection("instrument", boost::bind(&CoulombCounter::parseReceivedBytes, this, _1));
}

void CoulombCounter::setupPublishers()
{
  ds_base::SensorBase::setupPublishers();

  DS_D(CoulombCounter);

  // publish to charge topic
  d->charge_pub_ = nodeHandle().advertise<ds_hotel_msgs::Charge>(ros::this_node::getName() + "/charge", 10);
}

void CoulombCounter::setupParameters()
{
  ds_base::SensorBase::setupParameters();

  DS_D(CoulombCounter);
}

void CoulombCounter::setupServices()
{
  ds_base::SensorBase::setupServices();

  DS_D(CoulombCounter);

  auto nh = nodeHandle();

  // this binds any service request to the send command callback
  cmd_service = nh.advertiseService<ds_hotel_msgs::ChargeCmd::Request, ds_hotel_msgs::ChargeCmd::Response>(
      ros::this_node::getName() + "/cmd", boost::bind(&CoulombCounter::send_cmds, this, _1, _2));
}

// parse command message and send significant messages to board
bool CoulombCounter::send_cmds(ds_hotel_msgs::ChargeCmd::Request& req, ds_hotel_msgs::ChargeCmd::Response& resp)
{
  DS_D(CoulombCounter);

  // send frequency command if changed
  if (req.frequency != frequency)
  {
    // update
    frequency = req.frequency;
    // send command accordingly
    if (req.frequency == 1)
    {
      board_connection->send("f1\n");
    }
    else if (req.frequency == 2)
    {
      board_connection->send("f2\n");
    }
    else if (req.frequency == 3)
    {
      board_connection->send("f3\n");
    }
    else if (req.frequency == 4)
    {
      board_connection->send("f4\n");
    }
  }

  // send appropriate channel reset commands
  if (req.reset[0] == 1)
  {
    board_connection->send("r1\n");
  }
  if (req.reset[1] == 1)
  {
    board_connection->send("r2\n");
  }
  if (req.reset[2] == 1)
  {
    board_connection->send("r3\n");
  }
  if (req.reset[3] == 1)
  {
    board_connection->send("r4\n");
  }

  // change shunt values in firmware from default, only when the requested value has changed
  if (req.shunt_value[0] != 0 && req.shunt_value[0] != shunt_1)
  {
    shunt_1 = req.shunt_value[0];
    std::stringstream v;
    v << shunt_1;
    board_connection->send("s1\n");
    board_connection->send(v.str() + "\n");
  }

  if (req.shunt_value[1] != 0 && req.shunt_value[1] != shunt_2)
  {
    shunt_2 = req.shunt_value[1];
    std::stringstream v;
    v << shunt_2;
    board_connection->send("s2\n");
    board_connection->send(v.str() + "\n");
  }

  if (req.shunt_value[2] != 0 && req.shunt_value[2] != shunt_3)
  {
    shunt_3 = req.shunt_value[2];
    std::stringstream v;
    v << shunt_3;
    board_connection->send("s3\n");
    board_connection->send(v.str() + "\n");
  }

  if (req.shunt_value[3] != 0 && req.shunt_value[3] != shunt_4)
  {
    shunt_4 = req.shunt_value[3];
    std::stringstream v;
    v << shunt_4;
    board_connection->send("s4\n");
    board_connection->send(v.str() + "\n");
  }

  return true;
}

std::pair<bool, ds_hotel_msgs::Charge> CoulombCounter::parse_charge(const ds_core_msgs::RawData& bytes)
{
  auto _msg = ds_hotel_msgs::Charge{};
  char c;

  // parse four floating point values
  const auto n_parsed = sscanf(reinterpret_cast<const char*>(bytes.data.data()), "%f %f %f %f", &_msg.charge[0],
                               &_msg.charge[1], &_msg.charge[2], &_msg.charge[3]);

  if (n_parsed < 4 || n_parsed > 4)
  {
    ROS_DEBUG("Expected 4 values, found %d", n_parsed);
    return { false, _msg };
  }

  // this will typically return to a call from publish_charge, where these values are published to a relevant topic
  _msg.ds_header = bytes.ds_header;
  _msg.header.stamp = _msg.ds_header.io_time;

  return { true, _msg };
}

// sends raw charge messages to parse_charge and publishes parsed messages to topic
void CoulombCounter::parseReceivedBytes(const ds_core_msgs::RawData& bytes)
{
  auto ok = false;
  auto msg = ds_hotel_msgs::Charge{};

  std::tie(ok, msg) = CoulombCounter::parse_charge(bytes);

  // exit when fewer than 4 values were parsed from message
  if (!ok)
  {
    return;
  }

  // populate header
  FILL_SENSOR_HDR_IOTIME(msg, msg.ds_header.io_time);

  // publish to charge topic
  DS_D(CoulombCounter);
  d->charge_pub_.publish(msg);
  updateTimestamp("/charge", msg.header.stamp);
}
}
