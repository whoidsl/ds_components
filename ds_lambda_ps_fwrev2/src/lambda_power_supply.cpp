/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 1/17/18.
//

#include "lambda_power_supply_private.h"

using namespace ds_components;

// Protected 'default' constructor
LambdaPowerSupply::LambdaPowerSupply()
  : ds_base::DsProcess(), d_ptr_(std::unique_ptr<LambdaPowerSupplyPrivate>(new LambdaPowerSupplyPrivate(this)))
{
  // do nothing
}

// Protected constructor with arguments for ros::init
LambdaPowerSupply::LambdaPowerSupply(int argc, char* argv[], const std::string& name)
  : ds_base::DsProcess(argc, argv, name)
  , d_ptr_(std::unique_ptr<LambdaPowerSupplyPrivate>(new LambdaPowerSupplyPrivate(this)))
{
  // do nothing
}

LambdaPowerSupply::~LambdaPowerSupply() = default;

void LambdaPowerSupply::setupConnections()
{
  ds_base::DsProcess::setupConnections();

  DS_D(LambdaPowerSupply);
  d->iosm = addIoSM("statemachine", "instrument");

  // Setup our commands
  std::stringstream address_cmd;
  address_cmd << "INST:SEL " << d_func()->address_ << "\r";
  d->address_command_ = ds_asio::IoCommand(address_cmd.str(), 0.250, false);
  d->iosm->addRegularCommand(d_func()->address_command_);
  d->_iosm_polling("MEAS:VOLT?\r");
  d->_iosm_polling(":VOLT?\r");
  d->_iosm_polling("MEAS:CURR?\r");
  d->_iosm_polling(":CURR?\r");
  d->_iosm_polling("OUTP:STAT?\r");
  d->_iosm_polling("SOUR:MOD?\r");
  d->_iosm_polling("SYST:SET?\r");
  d->_iosm_polling("STAT:QUES:COND?\r");
  d->_iosm_polling("SYST:ERR?\r");
}

void LambdaPowerSupply::setupPublishers()
{
  ds_base::DsProcess::setupPublishers();

  DS_D(LambdaPowerSupply);

  auto nh = nodeHandle();
  d->ds_ps_publisher = nh.advertise<ds_hotel_msgs::PowerSupply>(ros::this_node::getName() + "/dcpwr", 10);

  d->cmd_serv_ =
      nh.advertiseService<ds_hotel_msgs::PowerSupplyCommand::Request, ds_hotel_msgs::PowerSupplyCommand::Response>(
          ros::this_node::getName() + "/cmd", boost::bind(&LambdaPowerSupplyPrivate::_service_req, d, _1, _2));
}

void LambdaPowerSupply::setupParameters()
{
  ds_base::DsProcess::setupParameters();

  DS_D(LambdaPowerSupply);
  d->message_timeout_ = ros::Duration(ros::param::param<double>("~message_timeout", 30));
  d->address_ = ros::param::param<int>("~address", 6);

  // Load our parameter limits
  d->min_voltage_limit_ = ros::param::param<float>("~min_voltage_limit", 0);
  d->max_voltage_limit_ = ros::param::param<float>("~max_voltage_limit", 10000.0);
  d->min_current_limit_ = ros::param::param<float>("~min_current_limit", 0);
  d->max_current_limit_ = ros::param::param<float>("~max_current_limit", 10000.0);

  // parse our IDnum.  Assume our addr is either XR# or GRD/XR#
  boost::regex idnumFind{ "([1-9]{1})" };
  boost::smatch idnum_find;
  boost::regex shoreFind{ "shore" };
  boost::smatch shore_find;
  if (boost::regex_search(ros::this_node::getName(), shore_find, shoreFind))
  {
    ROS_INFO_STREAM("This node is shore, so idnum_ = -1");
    d->idnum_ = -1;
  }
  else if (!boost::regex_search(ros::this_node::getName(), idnum_find, idnumFind))
  {
    ROS_ERROR_STREAM("IDNUM not found!");
  }
  else
  {
    d->idnum_ = std::strtoul(idnum_find[1].str().c_str(), nullptr, 10);
  }

  auto generated_uuid = ds_base::generateUuid("lambda_sdk_" + descriptiveName());
}

const std::string LambdaPowerSupplyPrivate::PARSE_REGEX_STR = "MV\\(([0-9\\.]+)\\),PV\\(([0-9\\.]+)\\),MC\\(([0-9\\.]+)"
                                                              "\\),PC\\(([0-9\\.]+)\\),SR\\(([0-9A-F]+)\\),FR\\(([0-9A-"
                                                              "F]+)\\)";
