/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 1/17/18.
//

#ifndef PROJECT_LAMBDAPOWERSUPPLY_PRIVATE_H
#define PROJECT_LAMBDAPOWERSUPPLY_PRIVATE_H

#include <ds_lambda_ps_fwrev2/lambda_power_supply.h>
#include <ds_core_msgs/Status.h>
#include <ds_core_msgs/RawData.h>
#include <ds_hotel_msgs/PowerSupply.h>
#include <ds_hotel_msgs/PowerSupplyCommand.h>

#include <sstream>
#include <iomanip>

namespace ds_components
{
struct LambdaPowerSupplyPrivate
{
  LambdaPowerSupplyPrivate(LambdaPowerSupply* base) : address_command_(ds_asio::IoCommand(1.0)), q_ptr_(base)
  {
  }
  virtual ~LambdaPowerSupplyPrivate() = default;
  DS_DISABLE_COPY(LambdaPowerSupplyPrivate);
  DS_DECLARE_PUBLIC(LambdaPowerSupply);

  const static std::string PARSE_REGEX_STR;

  bool _service_req(const ds_hotel_msgs::PowerSupplyCommand::Request& req,
                    ds_hotel_msgs::PowerSupplyCommand::Response& resp)
  {
    ROS_ERROR_STREAM_ONCE("LAMBDA SERVICE RECEIVED v:" << req.prog_volts << " c:" << req.prog_amps);

    // (possibly) turn off the output
    if (!req.output_enable)
    {
      send_command("OUTP:STAT 0\r");
    }

    // set voltage
    if (req.prog_volts != req.POWERSUPPLY_NODATA && last_message_.prog_volts != req.prog_volts &&
        req.prog_volts <= max_voltage_limit_ && req.prog_volts >= min_voltage_limit_)
    {
      std::stringstream cmd;
      cmd << ":VOLT " << std::fixed << std::setprecision(3) << req.prog_volts << "\r";
      send_command(cmd.str());
    }

    // set voltage
    if (req.prog_amps != req.POWERSUPPLY_NODATA && last_message_.prog_amps != req.prog_amps &&
        req.prog_amps <= max_current_limit_ && req.prog_amps >= min_current_limit_)
    {
      std::stringstream cmd;
      cmd << ":CURR " << std::fixed << std::setprecision(3) << req.prog_amps << "\r";
      send_command(cmd.str());
    }

    // (possibly) turn on the output
    if (req.output_enable)
    {
      send_command("OUTP:STAT 1\r");
    }

    // Set the user lockout
    if (req.frontpanel_locked)
    {
      if (req.frontpanel_locked)
      {
        send_command("SYST:SET LLO\r");
      }
      else
      {
        send_command("SYST:SET LOC\r");
      }
    }

    resp.success = 1;
    return true;
  }

  bool _command_result_recv(const std::string& cmdstr, const ds_core_msgs::RawData& bytes)
  {
    std::string msg(reinterpret_cast<const char*>(bytes.data.data()), bytes.data.size() - 1);
    ROS_ERROR_STREAM("MSG FROM PS: "<<msg);
    if (msg == "0, No error")
    {
      return true;
    }
    std::string cmd = cmdstr.substr(0, cmdstr.size() - 1);
    ROS_ERROR_STREAM("Got unexpected Lambda Power Supply Result for command: \"" << cmd << "\" : \"" << msg << "\"");
    return true;
  }

  void send_command(const std::string& cmdstr)
  {
    ROS_ERROR_STREAM_ONCE("Lambda cmd sent:" << cmdstr);
    iosm->addPreemptCommand(address_command_);
    iosm->addPreemptCommand(ds_asio::IoCommand(
        cmdstr, 0.250, true, boost::bind(&LambdaPowerSupplyPrivate::_command_result_recv, this, cmdstr, _1)));
    ROS_INFO_STREAM("Queued command: " <<cmdstr);
  }
  
  bool _poll_result_recv(const std::string& cmdstr, const ds_core_msgs::RawData& bytes)
  {
    ds_hotel_msgs::PowerSupply ret;
    std::string msg(reinterpret_cast<const char*>(bytes.data.data()), bytes.data.size() - 1);
    //ROS_ERROR_STREAM("POLL MSG FROM PS: "<<msg);
    last_message_timestamp_ = bytes.header.stamp;
    ret.ds_header = bytes.ds_header;
    ret.header.stamp = ret.ds_header.io_time;

    if (cmdstr == "MEAS:VOLT?\r") {
      meas_volts_ = strtof(msg.c_str(), NULL);;
    }
    else if (cmdstr == ":VOLT?\r") {
      prog_volts_ = strtof(msg.c_str(), NULL);;
    }
    else if (cmdstr == "MEAS:CURR?\r") {
      meas_amps_ = strtof(msg.c_str(), NULL);;
    }
    else if (cmdstr == ":CURR?\r") {
      prog_amps_ = strtof(msg.c_str(), NULL);;
    }
    else if (cmdstr == "OUTP:STAT?\r") {
      if (msg != "OFF") {
        output_enable_ = true;
      }
      else {
        output_enable_ = false;
      }  
    }
    else if (cmdstr == "SOUR:MOD?\r") {
      if (msg != "OFF") {
        if (msg == "CV") {
          constant_voltage_ = true;
          constant_current_ = false;
        }
        else if (msg == "CC") {
          constant_current_ = true;
          constant_voltage_ = false;
        }
      }
      else {
        constant_current_ = false;
        constant_voltage_ = false;
      }
    }
    else if (cmdstr == "SYST:SET?\r") {
      if (msg == "LLO") {
        frontpanel_locked_ = true;
      }
      else if (msg == "REM") {
        frontpanel_locked_ = true;
      }
      else {
        frontpanel_locked_ = false;
      }  
    }
    else if (cmdstr == "STAT:QUES:COND?\r") {
      if (std::stoi(msg, nullptr, 0) != 00000) {
        fault_ = true;
      }
      else {
        fault_ = false;
      }      
    }
    else if (cmdstr == "SYST:ERR?\r") {
      status_msg_ = msg;
    }
    else {
      ROS_ERROR_STREAM("BAD CMD SENT TO POWER SUPPLY, CANNOT PARSE MSG!");
      return false;
    }

    //FILL IN THE MESSAGE FIELDS
    std::copy(std::begin(uuid_.data), std::end(uuid_.data), std::begin(ret.ds_header.source_uuid));
    ret.idnum = idnum_;
    ret.status_msg = status_msg_;
    ret.meas_volts = meas_volts_;
    ret.prog_volts = prog_volts_;
    ret.meas_amps = meas_amps_;
    ret.prog_amps = prog_amps_;
    ret.output_enable = output_enable_;
    if (!ret.fault && ret.output_enable) {
      ret.status_good = true;
    }
    else {
      ret.status_good = false;
    }
    ret.fault = fault_;
    ret.frontpanel_locked = frontpanel_locked_;
    ret.constant_voltage = constant_voltage_;
    ret.constant_current = constant_current_;
    last_message_ = ret;
    ds_ps_publisher.publish(ret); //PUBLISH
    return true;
  }

  void _iosm_polling(const std::string& cmdstr) {
      // Setup our polling commands
      iosm->addPreemptCommand(address_command_);
      iosm->addRegularCommand(ds_asio::IoCommand(
        cmdstr, 0.250, true, boost::bind(&LambdaPowerSupplyPrivate::_poll_result_recv, this, cmdstr, _1)));
      iosm->addRegularCommand(ds_asio::IoCommand(1.0)); //Timing control so the powersupply isn't overloaded
  }

  /// @brief Power supply publisher
  ros::Publisher ds_ps_publisher;

  /// @brief Command service handler
  ros::ServiceServer cmd_serv_;

  /// @brief I/O state machine
  boost::shared_ptr<ds_asio::IoSM> iosm;

  /// @brief The Lambda power supply line needs an address command before every real command
  ds_asio::IoCommand address_command_;

  /// @brief The power supply UUID
  boost::uuids::uuid uuid_;

  /// @brief The timestamp of the last message from the I/O state machine
  ros::Time last_message_timestamp_;

  ds_hotel_msgs::PowerSupply last_message_;

  /// @brief The time duration between valid messages to consider bad
  ros::Duration message_timeout_;

  /// @brief Address of the power supply on the bus
  int address_;

  /// @brief ID number for debugging purposes (Human usable, not necessarily same as address_
  int idnum_;

  /// @brief Hard minimum voltage limit imposed by parameter server (for safety!)
  float min_voltage_limit_;

  /// @brief Hard maximum voltage limit imposed by parameter server (for safety!)
  float max_voltage_limit_;

  /// @brief Hard minimum current limit imposed by parameter server (for safety!)
  float min_current_limit_;

  /// @brief Hard maximum current limit imposed by parameter server (for safety!)
  float max_current_limit_;

  /// @brief Variables to contain power supply info before it is put into a ROS msg
  float meas_volts_;
  float prog_volts_;
  float meas_amps_;
  float prog_amps_;
  bool output_enable_;
  bool constant_voltage_;
  bool constant_current_;
  bool frontpanel_locked_;
  std::string status_msg_;
  bool fault_;

  LambdaPowerSupply* q_ptr_;
};
}

#endif  // PROJECT_LAMBDAPOWERSUPPLY_PRIVATE_H
