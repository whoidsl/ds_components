# Driver for the TDK-Lambda Rack-Mount DC Power Supplies Running FW Revision 2.1 or higher

## Note:
This is a new driver only for TDK-Lambda supplies running firmware version 2.1 or greater. It uses a completely different command set than the other TDK-Lambda driver (ds_lambda_ps).

To differentiate:
* This driver uses a set of status commands rather than a single status command.
* This driver uses OUTP:STAT 0 and OUTP:STAT 1 rather than OUT 0 or OUT 1
* This driver uses INST:SEL instead of ADR

These are just some of the command set differences, but should help easily determine which driver you are using and whether or not it is the right one.