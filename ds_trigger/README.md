# Scope
The purpose of the ds_trigger driver is to control and reconfigure a DSL vehicle's trigger board. It can read and set overall board options as well as every sonar channels' options. This document aims to explain the services and topics the ds_trigger driver exposes to clients. It assumes that the driver was launched in the `/sentry/sonars` namespace and that all `ros*` commands are launched in a ROS-sourced catkin workspace.

# Running the driver:
```bash
$ roslaunch ds_trigger trigger.launch
```
# To interact with the driver on the command line:
# 0. Preparing the environment
```bash
$ cd <catkin_workspace>
$ source devel/setup.bash
```
Within `config/`, you'll see yaml files that can be used to configure sonar channels' trigger timing, general trigger board settings, and driver settings. Here's a snippet from `mission_trigger_configuration.yaml`, which is a copy of `sentry_config/config/trigger/mission_trigger_configuration.yaml`, the actual file intended for use in dives:
```yaml
instrument: 
    type: SERIAL
    port: /dev/ttyS31     # For benchtest scenarios, this will usually be /dev/ttyUSB*
    baud: 9600
    data_bits: 8
    parity: none
    stopbits: 1
    matcher: match_char
    delimiter: "03"     # This is the hex value that the driver interprets as end-of-transmission
                        # for data coming from the trigger board. If this isn't correct, the asynchronous I/O callback function
                        # will be called prematurely or potentially not at all. So far this value has worked for all 
                        # trigger board firmware revisions, but that could change. 

# The lists below tell the driver what parameters on the param server and trigger board should be compared. The benefit of 
# parameterizing the params to be compared is that it's easy to adapt this driver for different trigger board firmware revisions.
board_params: [ "sync", "freerun", "oneshot", "autostart", "mute", "baudrate", "fw_version", "serial_number" ] # Overall board configuration data
channel_params: [ "name", "period", "polarity", "duration", "delays", "exclude_from", "exclude_duration" ]  # Configurable options for each sonar device/channel.
configurable_params: ["sync", "freerun", "oneshot", "autostart", "mute" ]   # Same as overall board configuration data, this is the subset that 
                                                                            # can be changed during normal runtime. 

```

# 1. Enabling the board
```bash
$ rosservice call /sentry/sonars/trigger/board_enablement "data: true" 
```
 To verify if the call succeeded or not, run `rostopic echo /sentry/sonars/trigger/board/status` in another ROS-sourced terminal on the same host. If you see `data: "running"` or `data: "waiting"` echoed, the service call has succeeded. The service call return message will say "running", but this isn't absolutely guaranteed to be correct, unlike the topic subscription approach.
 
# 2. Disabling the board  
```bash
$ rosservice call /sentry/sonars/trigger/board_enablement "data: false"
```
To verify if the call succeeded or not, run `rostopic echo /sentry/sonars/trigger/board/status` in another ROS-sourced terminal on the same host. If you see `data: "stopped"` echoed, the service call has succeeded. The service call return message will say "stopped", but this isn't absolutely guaranteed to be correct, unlike the topic subscription approach.
# 3. Comparing the board's configuration against the parameter server  
```bash
$ rosservice call /sentry/sonars/trigger/check_params "command: 1   
channels_to_change:  
  - name: ''  
    specified_params:  
      - key: ''  
        value: ''  
global_params_to_change:  
  - key: ''  
    value: ''
apply: false"        # Change apply to true if you want to force the parameter server configuration onto the 
                     # board in the event a mismatch is found
```
This service takes relatively long to complete, so the answer (a simple boolean) is not returned in the service response itself, but rather through the rostopic `/sentry/sonars/trigger/board/params_matching`. With `apply` set to false, the topic can return `data: False` or `data: True`. When `apply` is true `data` should echo true, either because the param server's configuration was downloaded and saved on the board or there was no difference between the server and the board to begin with. 
# 4. Installing configuration changes to the board.  
```bash
$ rosservice call /sentry/sonars/trigger/install_configuration "command: 0  
channels_to_change:              # Can change up to 8 channels in a single request! Channel name changing is not supported, though.  
  - name: '<channel name here>'  
    specified_params:  
      - key: 'num_delays'           # Can be any valid channel parameter, specify as many as desired.  
        value: '3'  
      - key: 'delays'  
        value: '[250,500,750]'  
global_params_to_change:  
  - key: 'oneshot'                # Can be any valid board parameter, specify as many as desired.  
    value: 'false'"     
apply: false"                     # Does not matter whether apply is true here or not.
```
This service is intended for making live changes to the sonars' triggering parameters during dives. As of right now, this service does not verify that requested changes have been acknowledged and administered by the trigger board's firmware.

# 5. Dis/Enabling individual channels
Suppose you wanted the "Pioneer" and "EM2040" channels enabled, and the "Reson" channel disabled. Here's what the service call would be:
```bash
$ rosservice call /sentry/sonars/trigger/channel_enablement "command: 0
channels_to_change:
- name: 'Pioneer'
  enable: true 
  specified_params:
  - key: ''
    value: ''
- name: 'Reson'
  enable: false
  specified_params:
  - key: ''
    value: ''
- name: 'EM2040'
  enable: true 
  specified_params:
  - key: ''
    value: ''
global_params_to_change:
- key: ''
  value: ''
apply: false" 
is_successful: True
action_done: 0
reason: ''
```
Only the channel names and `enable` flags are used in the service. All other parameters are ignored. 

# To compile all test code in the Catkin workspace:
```bash
$ catkin_make -j 4 tests
```
# To run the Rostests for this driver:  
```bash
$ rostest ds_trigger trigger.test
```
# Here's the tree of ds_trigger:  
```
├── build  
├── cmake  
│   └── FindClangFormat.cmake                     # I don't know what this does  
├── CMakeLists.txt                  
├── config    
│   ├── mission_trigger_configuration.yaml        # The reconfigurable version for a specific dive
│   ├── sentry_trigger_config_May52021.yaml         # Copy of an actual Sentry config
│   ├── simulator_trigger_configuration.yaml      # The parameter standard to compare against during trigger board testing  
│   └── standard_trigger_configuration.yaml       # The parameter standard to compare against during driver development 
├── devel                                        
├── launch  
│   └── trigger.launch  # Sets up trigger board parameters on the param server  
├── package.xml                      
├── README.md  
├── src  
│   ├── ds_trigger                 # Folder holding the driver  
│   │   ├── ds_trigger.cpp         # The trigger board driver (library)
│   │   └── ds_trigger.h           # " "  
│   ├── ds_trigger_node.cpp        # Node that runs the driver.   
│   └── test
│       └── ds_trigger_test_node.cpp  # Node that runs the rostests
├── test
│   └── trigger.test               # Rostest launch file
└── troubleshooting.md             # Guide written to help at-sea engineers troubleshoot a seg-fault issue in the trigger driver.
```
