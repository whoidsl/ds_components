# Trigger Driver Troubleshooting Guide

## Scope
The purpose of this document is to explain how to reproduce (or at least attempt to reproduce) and record a segmentation fault caused by certain service calls to this driver. The known affected version is commit hash 876cb. These instruction sections are laid out in chronological order.

## Setting up the Testing Environment
The tester will need four terminals on Sentry's mainstack for this effort.

 * One to launch the driver (**DRIVER**)
 * One to make the suspect service call (**CALL**)
 * One to monitor the topic data published by the driver (**ECHO**)
 * One recommended for general command-line work (**ETC**)

### Core Dump Preparation
First ensure that the mainstack has core dumps enabled: `sentry@sentry$ ulimit -c`. If this command prints a 0, then core dumps are not enabled.
If core dumps aren't enabled, navigate to the **DRIVER** window and run `ulimit -c unlimited`. This command allows core dumps to be generated with no limit on their size. 

### Prepping and Launching the Trigger Driver
Ensure you're in the **DRIVER** window and run the following:

```bash
cd ~/sentry_ws
source devel/setup.bash
pushd src/ds_components/ds_trigger/
git checkout 876cb0c
popd
catkin_make -j 4
vim src/ds_components/ds_trigger/launch/trigger.launch 
# In the vim session, press <Esc> to enter command mode 
# and then type in :%s/standard/mission/g to set the launchfile up to load the right yaml file.
# Save and exit: <Esc> and then :wq
# Now time to launch!
roslaunch ds_trigger trigger.launch
```

When the driver node spins up, it'll print PID info to the console. Here's example output:
```
  /
    ds_trigger (ds_trigger/ds_trigger_node)

auto-starting new master
process[master]: started with pid [12843]
ROS_MASTER_URI=http://localhost:11311

setting /run_id to 4b1c1162-a2b3-11eb-86fa-04ed338de8f7
process[rosout-1]: started with pid [12865]
started core service [/rosout]
process[ds_trigger-2]: started with pid [12872]
[ INFO] [1619017651.892828147]: Disabling period
```

In the **ETC** window, run `ls -l /proc/<pid>/cwd` for each pid that appeared; they should all point to `/home/sentry/.ros/`. This will be the directory the core dumps end up in.
Also take note of the `run_id`. We'll need it later.

### Echoing Published Driver Data
This is the simplest one. After navigating to the **ECHO** window, run the following:

```bash
cd ~/sentry_ws
source devel/setup.bash
rostopic echo /ds_trigger/board/params_matching
```

### Making the Service Call 

Navigate to the **CALL** window and run the following: 
```bash
cd ~/sentry_ws
source devel/setup.bash
# Then, type in rosservice call /ds_trigger/check_params <TAB>. 
# After the tab-complete, your command should look like: 
sentry@sentry:~/sentry_ws$ rosservice call /ds_trigger/check_params "command: 0
channels_to_change:
- name: ''
  specified_params:
  - key: ''
    value: ''
global_params_to_change:
- key: ''
  value: ''
apply: false" 

# Modify the tab-completed command to look like so:
sentry@sentry:~/sentry_ws$ rosservice call /ds_trigger/check_params "command: 1
channels_to_change:
- name: ''
  specified_params:
  - key: ''
    value: ''
global_params_to_change:
- key: ''
  value: ''
apply: false" 
# There are many ways to call this service, so I may ask you to run more 
# incantations later. We'll start with this simple invocation.
```
Whenever you're ready, hit `Enter` to make the service call. Wait a second, and then flip to the **DRIVER** window. Has it crashed? If it has, great- if not, logs are still valuable to fetch. Switch over to the **ECHO** window- did it print a result? If so, what was the result?

When you're done executing the service call and scanning the other windows, go ahead and ctrl-c the processes running in the **DRIVER**, **CALL**, and **ECHO** windows.

## Assembling the Records

I unfortunately can't remember the exact service call invocation that led to the driver crash. Even if this test procedure didn't produce a segfault, there's valuable information to be gleaned from the roslogs. Remember the `run_id` from before? We're going to use it now. Copy that value into your clipboard and then run the following command to zip up the desired log data:
```bash
zip -r Trigger_Driver_Roslogs /home/joe/.ros/log/<run_id>/
```
That will create `Trigger_Driver_Roslogs.zip` in your `pwd`. Please email that data to me. 

There's also a minute chance that the logs from the testing I did in February are still around on the mainstack. To check that:
```bash
cd ~/.ros/
find . -newermt '05 feb 2021 00:00:00' -not -newermt '12 feb 2021 00:00:00'
```
If that query turns up anything, please send that data my way, too.

And last but definitely not least, if the driver crashed and generated a core dump, that dump should be located in `/home/sentry/.ros` (or whatever the `cwd` was that you found in the `/proc` filesystem earlier). The core file should have a filename starting with "core.". Here's a command to locate the core dump assuming you run all the testing on April 22nd:
```bash
find . -iname "core*" -newermt '22 apr 2021 00:00:00' -not -newermt '23 apr 2021 00:00:00'
```

