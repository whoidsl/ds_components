/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/

#include "../ds_trigger/ds_trigger.h"
#include <gtest/gtest.h>
#include <std_srvs/SetBool.h>

#include <ros/console.h>
#include <log4cxx/logger.h>

// TO BUILD THIS FILE: catkin_make -j 4 tests -DCATKIN_BLACKLIST_PACKAGES="sail_grd;ds_ros_examples"
// TO RUN THESE ROSTESTS: catkin_make run_tests_ds_trigger_rostest
const char* VALID_BOARD_JSON = R"(
{
	"status": "invalid",
	"channels": [{
			"name": "Channel 0",
			"enabled": true,
			"valid": true,
			"period": 500,
			"polarity": 1,
			"duration": 50,
			"num_delays": 1,
			"delays": [250],
			"exclude_from": 0,
			"exclude_duration": 0
		},
		{
			"name": "Channel 1",
			"enabled": true,
			"valid": true,
			"period": 500,
			"polarity": 1,
			"duration": 50,
			"num_delays": 1,
			"delays": [250],
			"exclude_from": 0,
			"exclude_duration": 0
		},
		{
			"name": "Channel 2",
			"enabled": true,
			"valid": true,
			"period": 500,
			"polarity": 1,
			"duration": 50,
			"num_delays": 1,
			"delays": [250],
			"exclude_from": 0,
			"exclude_duration": 0
		},
		{
			"name": "Channel 3",
			"enabled": true,
			"valid": true,
			"period": 500,
			"polarity": 1,
			"duration": 50,
			"num_delays": 1,
			"delays": [250],
			"exclude_from": 0,
			"exclude_duration": 0
		},
		{
			"name": "Channel 4",
			"enabled": true,
			"valid": true,
			"period": 500,
			"polarity": 1,
			"duration": 50,
			"num_delays": 1,
			"delays": [250],
			"exclude_from": 0,
			"exclude_duration": 0
		},
		{
			"name": "Channel 5",
			"enabled": true,
			"valid": true,
			"period": 500,
			"polarity": 1,
			"duration": 50,
			"num_delays": 1,
			"delays": [250],
			"exclude_from": 0,
			"exclude_duration": 0
		},
		{
			"name": "Channel 6",
			"enabled": true,
			"valid": true,
			"period": 500,
			"polarity": 1,
			"duration": 50,
			"num_delays": 1,
			"delays": [250],
			"exclude_from": 0,
			"exclude_duration": 0
		},
		{
			"name": "Channel 7",
			"enabled": true,
			"valid": true,
			"period": 500,
			"polarity": 1,
			"duration": 50,
			"num_delays": 1,
			"delays": [250],
			"exclude_from": 0,
			"exclude_duration": 0
		}
	],
	"sync": false,
	"sync_polarity": 1,
	"freerun": true,
	"oneshot": false,
	"autostart": false,
	"mute": false,
	"baudrate": 9600,
	"wdt_events": 0,
	"brownout_events": 0,
	"fw_version": "3.1.0",
	"serial_number": 6
})";

const char* PARAM_SERVER_MATCH = R"(settings
{
	"channels": [{
			"name": "Channel 0",
			"enabled": true,
			"valid": true,
			"period": 500,
			"polarity": 1,
			"duration": 50,
			"num_delays": 2,
			"delays": "[100,250]",
			"exclude_from": 0,
			"exclude_duration": 0
		},
		{
			"name": "Channel 1",
			"enabled": true,
			"valid": true,
			"period": 500,
			"polarity": 1,
			"duration": 50,
			"num_delays": 1,
			"delays": "[250]",
			"exclude_from": 0,
			"exclude_duration": 0
		},
		{
			"name": "Channel 2",
			"enabled": true,
			"valid": true,
			"period": 500,
			"polarity": 1,
			"duration": 50,
			"num_delays": 1,
			"delays": "[250]",
			"exclude_from": 0,
			"exclude_duration": 0
		},
		{
			"name": "Channel 3",
			"enabled": true,
			"valid": true,
			"period": 500,
			"polarity": 1,
			"duration": 50,
			"num_delays": 1,
			"delays": "[250]",
			"exclude_from": 0,
			"exclude_duration": 0
		},
		{
			"name": "Channel 4",
			"enabled": true,
			"valid": true,
			"period": 500,
			"polarity": 1,
			"duration": 50,
			"num_delays": 1,
			"delays": "[250]",
			"exclude_from": 0,
			"exclude_duration": 0
		},
		{
			"name": "Channel 5",
			"enabled": true,
			"valid": true,
			"period": 500,
			"polarity": 1,
			"duration": 50,
			"num_delays": 1,
			"delays": "[250]",
			"exclude_from": 0,
			"exclude_duration": 0
		},
		{
			"name": "Channel 6",
			"enabled": true,
			"valid": true,
			"period": 500,
			"polarity": 1,
			"duration": 50,
			"num_delays": 1,
			"delays": "[250]",
			"exclude_from": 0,
			"exclude_duration": 0
		},
		{
			"name": "Channel 7",
			"enabled": true,
			"valid": true,
			"period": 500,
			"polarity": 1,
			"duration": 50,
			"num_delays": 1,
			"delays": "[250]",
			"exclude_from": 0,
			"exclude_duration": 0
		}
	],
	"status": "invalid",
	"sync": false,
	"sync_polarity": 0,
	"freerun": true,
	"oneshot": false,
	"autostart": false,
	"mute": false,
	"baudrate": 9600,
	"fw_version": "3.1.0",
	"serial_number": 6
})";

const char* VALID_JSON = R"(
{
	"status": "invalid",
	"channels": [{
			"name": "Channel 0",
			"enabled": true,
			"valid": true,
			"period": 500,
			"polarity": 1,
			"duration": 50,
			"num_delays": 1,
			"delays": [250],
			"exclude_from": 0,
			"exclude_duration": 0
		},
		{
			"name": "Channel 1",
			"enabled": true,
			"valid": true,
			"period": 500,
			"polarity": 1,
			"duration": 50,
			"num_delays": 1,
			"delays": [250],
			"exclude_from": 0,
			"exclude_duration": 0
		},
		{
			"name": "Channel 2",
			"enabled": true,
			"valid": true,
			"period": 500,
			"polarity": 1,
			"duration": 50,
			"num_delays": 1,
			"delays": [250],
			"exclude_from": 0,
			"exclude_duration": 0
		},
		{
			"name": "Channel 3",
			"enabled": true,
			"valid": true,
			"period": 500,
			"polarity": 1,
			"duration": 50,
			"num_delays": 1,
			"delays": [250],
			"exclude_from": 0,
			"exclude_duration": 0
		},
		{
			"name": "Channel 4",
			"enabled": true,
			"valid": true,
			"period": 500,
			"polarity": 1,
			"duration": 50,
			"num_delays": 1,
			"delays": [250],
			"exclude_from": 0,
			"exclude_duration": 0
		},
		{
			"name": "Channel 5",
			"enabled": true,
			"valid": true,
			"period": 500,
			"polarity": 1,
			"duration": 50,
			"num_delays": 1,
			"delays": [250],
			"exclude_from": 0,
			"exclude_duration": 0
		},
		{
			"name": "Channel 6",
			"enabled": true,
			"valid": true,
			"period": 500,
			"polarity": 1,
			"duration": 50,
			"num_delays": 1,
			"delays": [250],
			"exclude_from": 0,
			"exclude_duration": 0
		},
		{
			"name": "Channel 7",
			"enabled": true,
			"valid": true,
			"period": 500,
			"polarity": 1,
			"duration": 50,
			"num_delays": 1,
			"delays": [250],
			"exclude_from": 0,
			"exclude_duration": 0
		}
	],
	"sync": false,
	"sync_polarity": 1,
	"freerun": true,
	"oneshot": false,
	"autostart": false,
	"mute": false,
	"baudrate": 9600,
	"wdt_events": 0,
	"brownout_events": 0,
	"fw_version": "3.1.0",
	"serial_number": 6
})";

const char* RAW_DUMP_JSON = R"(
Using /dev/ttyUSB0: 9600 for trg board.
settings
{
"status": "invalid",
"channels": [
  null,
  null,
  null,
  null,
  null,
  null,
  null,
  null
],
"sync": false,
"sync_polarity": 1,
"freerun": true,
"oneshot": false,
"autostart": false,
"mute": false,
"baudrate": 9600,
"wdt_events": 0,
"brownout_events": 0,
"fw_version": "3.1.0",
"serial_number": 6
}

# TRG config generated on 2020-10-22 18:11:05.065529 UTC


[instrument]
baudrate=9600
bytesize=8
parity=N
port=/dev/ttyUSB0
stopbits=1


[general]
autostart=False
freerun=True
fw_version=3.1.0
mute=False
oneshot=False
serial_number=6
sync=False
sync_polarity=1
)";

const char* MISMATCHED_BRACKET_JSON = R"(
{
	"status": "invalid",
	"channels": [{
			"name": "Channel 0",
			"enabled": true,
			"valid": true,
			"period": 500,
			"polarity": 1,
			"duration": 50,
			"num_delays": 1,
			"delays": [250],
			"exclude_from": 0,
			"exclude_duration": 0
		},
		{
			"name": "Channel 1",
			"enabled": true,
			"valid": true,
			"period": 500,
			"polarity": 1,
			"duration": 50,
			"num_delays": 1,
			"delays": [250],
			"exclude_from": 0,
			"exclude_duration": 0
		},
		{
			"name": "Channel 2",
			"enabled": true,
			"valid": true,
			"period": 500,
			"polarity": 1,
			"duration": 50,
			"num_delays": 1,
			"delays": [250],
			"exclude_from": 0,
			"exclude_duration": 0
		},
		
			"name": "Channel 3",
			"enabled": true,
			"valid": true,
			"period": 500,
			"polarity": 1,
			"duration": 50,
			"num_delays": 1,
			"delays": [250],
			"exclude_from": 0,
			"exclude_duration": 0
		},
		{
			"name": "Channel 4",
			"enabled": true,
			"valid": true,
			"period": 500,
			"polarity": 1,
			"duration": 50,
			"num_delays": 1,
			"delays": [250],
			"exclude_from": 0,
			"exclude_duration": 0
		},
		{
			"name": "Channel 5",
			"enabled": true,
			"valid": true,
			"period": 500,
			"polarity": 1,
			"duration": 50,
			"num_delays": 1,
			"delays": [250],
			"exclude_from": 0,
			"exclude_duration": 0
		},
		{
			"name": "Channel 6",
			"enabled": true,
			"valid": true,
			"period": 500,
			"polarity": 1,
			"duration": 50,
			"num_delays": 1,
			"delays": [250],
			"exclude_from": 0,
			"exclude_duration": 0
		},
		{
			"name": "Channel 7",
			"enabled": true,
			"valid": true,
			"period": 500,
			"polarity": 1,
			"duration": 50,
			"num_delays": 1,
			"delays": [250],
			"exclude_from": 0,
			"exclude_duration": 0
		}
	],
	"sync": false,
	"sync_polarity": 1,
	"freerun": true,
	"oneshot": false,
	"autostart": false,
	"mute": false,
	"baudrate": 9600,
	"wdt_events": 0,
	"brownout_events": 0,
	"fw_version": "3.1.0",
	"serial_number": 6
})";

class DsTriggerUnderTest : public ds_components::Trigger {
	FRIEND_TEST(DsTriggerTest, ValidateValidJsonTest);
	FRIEND_TEST(DsTriggerTest, ValidateRawJSONDumpTest);
	FRIEND_TEST(DsTriggerTest, ValidatedMismatchedJsonTest);
	FRIEND_TEST(DsTriggerTest, InitialCheckParamsQueryTest);
	FRIEND_TEST(DsTriggerTest, QueryBoardButNoResponseTest);
	FRIEND_TEST(DsTriggerTest, QueryBoardPerfectParamMatchTest);
	FRIEND_TEST(DsTriggerTest, BoardEnablementTest);
	FRIEND_TEST(DsTriggerTest, BoardConfiguration_Test);
	
public:
 	void setJsonPayload(std::string val)
	{
		json_payload.str({});
		json_payload << val;
	}

	void wrapper_class_setup() {
		setupPublishers();
	}
};

class DsTriggerTest : public ::testing::Test
{
	protected:
		void SetUp() override
		{			  
			under_test.wrapper_class_setup();
		}

	ds_hotel_msgs::TrigPrmCtrlCmd::Request request;
	ds_hotel_msgs::TrigPrmCtrlCmd::Response response;
	DsTriggerUnderTest under_test;
  	ds_hotel_msgs::TrigPrmCtrlCmd srv;
	std_srvs::SetBool enablement;	
	std::string test_string;
};

// Feed a valid JSON message into the driver and see if the driver validates it.
TEST_F(DsTriggerTest, ValidateValidJsonTest) {
    std::string vessel(VALID_JSON);
    under_test.setJsonPayload(vessel);
    under_test.validateJson(response);
    
    EXPECT_EQ(response.is_successful, false);
    EXPECT_EQ(response.reason, "Valid JSON");
    
}

// Feed the driver with a somewhat-valid JSON message. It contains valid JSON and YAML data. Since this message doesn't end with a 
// "}", it fails the JSON test.
TEST_F(DsTriggerTest, ValidateRawJSONDumpTest) {
    std::string vessel(RAW_DUMP_JSON);
    under_test.setJsonPayload(vessel);
    under_test.validateJson(response);
   
    EXPECT_EQ(response.is_successful, false);
    EXPECT_EQ(response.reason, "Unrecognized JSON wrapper characters");
    
}
// Feed the driver an almost-perfect JSON message. The only mistake in the message is a missing "{" before Channel 3.
TEST_F(DsTriggerTest, ValidatedMismatchedJsonTest) {  
    std::string vessel(MISMATCHED_BRACKET_JSON);
    under_test.setJsonPayload(vessel);
    under_test.validateJson(response);
    
    EXPECT_EQ(response.is_successful, false);
    EXPECT_EQ(response.reason, "Mismatched JSON brackets");
    
}

// When the client first calls the Parameter-checking service, the driver lets it know that it will have to wait. A meaningful reply will be published once
// the board returns the necessary data.
TEST_F(DsTriggerTest, InitialCheckParamsQueryTest)
{
	srv.request.command = ds_hotel_msgs::TrigPrmCtrlCmd::Request::CHECK_PARAMS;
	under_test.checkParameters(srv.request, srv.response);

	EXPECT_EQ(srv.response.is_successful, false);
	EXPECT_EQ(srv.response.reason, "Query Initiated");
}

// Just like the InitialCheckParamsQueryTest, but we "let a little time pass" and simulate a scenario where the client issues another 
// service request before the first one is finished (and by finished, we mean the result of the service has been published 
// to <ds_trigger_node_namespace>/ds_trigger/board/params_matching).
TEST_F(DsTriggerTest, QueryBoardButNoResponseTest)
{
	// Send the initial query to the trigger board to initiate the conversation (driver)
	srv.request.command = ds_hotel_msgs::TrigPrmCtrlCmd::Request::CHECK_PARAMS;
	under_test.checkParameters(srv.request, srv.response);
	EXPECT_EQ(srv.response.is_successful, false);
	EXPECT_EQ(srv.response.reason, "Query Initiated");
	
	// With no query to the trigger and response back, let's see what json validation does
	under_test.checkParameters(srv.request, srv.response);
	EXPECT_EQ(srv.response.is_successful, false);
	EXPECT_EQ(srv.response.reason, "Query in progress");
}

// In this test case, we send the initial query, get the standard wait message "Query Initiated", fake a response from the board, process it, 
// and check whether or not the driver thinks the board's parameters match the param server's. In this case, they match. To experiment with this test
// case, modify ../../config/simulator_trigger_configuration.yaml and the global variable "PARAM_SERVER_MATCH" found near the beginning of this file.
TEST_F(DsTriggerTest, QueryBoardPerfectParamMatchTest)
{
	// Send the initial query to the trigger board to initiate the conversation (driver)
	srv.request.command = ds_hotel_msgs::TrigPrmCtrlCmd::Request::CHECK_PARAMS;
	under_test.checkParameters(srv.request, srv.response);
	//EXPECT_EQ(srv.response.is_successful, '\0');
	EXPECT_EQ(srv.response.reason, "Query Initiated");
	
	// Fake a response from the trigger board and our processing of the data
	std::string vessel(PARAM_SERVER_MATCH);
	under_test.setJsonPayload(vessel);

	under_test.reportCheckParametersSuccess();
	under_test.reportMirroringSuccess();

	// With no query to the trigger and response back, let's see what json validation does
	EXPECT_EQ(under_test.didChannelParamsMatch(), true);  
	EXPECT_EQ(under_test.didBoardParamsMatch(), true); 
}

// Test the service that enables/disables all the channels on the trigger board.
TEST_F(DsTriggerTest, BoardEnablementTest)
{
	// Disable the trigger board
	enablement.request.data = false;
	under_test.boardEnablementControl(enablement.request, enablement.response);	
	EXPECT_EQ(enablement.response.message, "stopped");

	// Enable the trigger board 
	enablement.request.data = true;
	under_test.boardEnablementControl(enablement.request, enablement.response);
	EXPECT_EQ(enablement.response.message, "running");
}

// Test the service that downloads a board configuration from the parameter server and installs it on the board.
TEST_F(DsTriggerTest, BoardConfiguration_Test)
{
	srv.request.command == ds_hotel_msgs::TrigPrmCtrlCmd::Request::CONFIGURE_BOARD;
	under_test.installConfiguration(srv.request, srv.response);
	EXPECT_EQ(srv.response.is_successful, '\0');
}

int main(int argc, char* argv[])
{
  testing::InitGoogleTest(&argc, argv);
  ros::init(argc, argv, "ds_trigger_test_node");
  
  ROS_INFO("Successfully called a service.");
  auto ret = RUN_ALL_TESTS();
  ros::shutdown();

  return ret;
}
	


