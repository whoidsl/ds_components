/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef DS_TRIGGER_H
#define DS_TRIGGER_H

#include "ds_base/ds_process.h"
#include "ds_core_msgs/RawData.h"
#include "ds_base/sensor_base.h"

#include "ds_hotel_msgs/TrigPrmCtrlCmd.h"
//#include "ds_hotel_msgs/PwrSwitchCmd.h"
#include "std_srvs/SetBool.h"
#include "std_msgs/String.h"
#include "std_msgs/Bool.h"

#include <ds_base/util.h>
#include <vector>
#include <string>
#include <sstream>
#include <algorithm>
#include <json/json.h>
#include <boost/any.hpp>

namespace ds_components
{

class Trigger : public ds_base::SensorBase
{

public:
  explicit Trigger();
  Trigger(int argc, char* argv[], const std::string& name);
  ~Trigger() override;

  DS_DISABLE_COPY(Trigger);

protected:
  void setupConnections() override;
  void setupPublishers() override;
  void setupParameters() override;
  void setupServices() override;

  bool checkParameters(ds_hotel_msgs::TrigPrmCtrlCmd::Request& req, ds_hotel_msgs::TrigPrmCtrlCmd::Response& resp);
  bool boardEnablementControl(std_srvs::SetBoolRequest& req, std_srvs::SetBoolResponse& resp);
  bool channelEnablementControl(ds_hotel_msgs::TrigPrmCtrlCmd::Request& req, ds_hotel_msgs::TrigPrmCtrlCmd::Response& resp);
  bool installConfiguration(ds_hotel_msgs::TrigPrmCtrlCmd::Request& req, ds_hotel_msgs::TrigPrmCtrlCmd::Response& resp);
  bool compareGeneralBoardParams(ds_hotel_msgs::TrigPrmCtrlCmd::Request& req,ds_hotel_msgs::TrigPrmCtrlCmd::Response& resp);
  bool compareAllChannelParams(ds_hotel_msgs::TrigPrmCtrlCmd::Response& resp);
  bool validateJson(ds_hotel_msgs::TrigPrmCtrlCmd::Response& resp);
  bool didChannelParamsMatch() { return channel_params_matched;}
  bool didBoardParamsMatch() { return board_params_matched;}
  
  bool validateCommand(ds_hotel_msgs::TrigPrmCtrlCmd::Request& req, ds_hotel_msgs::TrigPrmCtrlCmd::Response& resp); 
  void sendCastedGlobalsToServer(std::string, ds_core_msgs::KeyString pair);
  void sendCastedChannelParamsToServer(XmlRpc::XmlRpcValue &channels_ref, ds_core_msgs::KeyString pair);
  void sendChannelParamsToServer(ds_hotel_msgs::TrigPrmCtrlCmd::Request& req,XmlRpc::XmlRpcValue &channels_ref);
  void sendGlobalParamsToBoard();
  void reportBoardEnabledStatus(); 
  void reportMirroringSuccess();
  void reportCheckParametersSuccess();
  void sendChannelParamsToBoard(XmlRpc::XmlRpcValue channels_config);
  void mirrorParamServerToBoard(bool board_params_matched, bool channel_params_matched);
  void markCheckParametersCompleted();
  void parseReceivedBytes(const ds_core_msgs::RawData& bytes) override;
  void sendToBoard(std::string cmdstr); 

  Json::Reader reader;
  Json::Value obj;
  std::stringstream json_payload;

private:

  bool channel_params_matched;
  bool board_params_matched;
  std::string string_terminator;
  
  boost::shared_ptr<ds_asio::DsConnection> board_connection;

  // for service advertisement
  ros::ServiceServer param_check_service;
  ros::ServiceServer board_enablement_service;
  ros::ServiceServer channel_enablement_service;
  ros::ServiceServer board_configuration_service;

  // Initialize those publishers
  ros::Publisher check_params_pub;
  ros::Publisher board_status_pub;

  bool active_query;
  bool removed_header;
  bool mirroring;

  std::vector<std::string> board_params;
  std::vector<std::string> channel_params;
  std::vector<std::string> configurable_params;
 

};
}

#endif

