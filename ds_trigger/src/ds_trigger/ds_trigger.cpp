/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*
*/

#include "json/config.h"
#include "json/value.h"
#include "ros/ros.h"
#include "ds_trigger.h"
#include "ds_core_msgs/RawData.h"
#include "ds_hotel_msgs/TrigPrmCtrlCmd.h"
#include "ds_hotel_msgs/TrigPrmCtrlCmd.h"
//#include "ds_hotel_msgs/PwrSwitchCmd.h"
#include <typeinfo>
#include <math.h>
#include <stdio.h>
#include <json/json.h>
#include <iostream>
#include <ios>
#include <fstream>
#include <map>
#include <memory>
#include <regex>

template <typename T> std::string type_name();

namespace ds_components
{
Trigger::Trigger() : ds_base::SensorBase(), active_query(false), removed_header(false), mirroring(false), 
			channel_params_matched(false), board_params_matched(false), board_connection(NULL)
{
}

Trigger::Trigger(int argc, char* argv[], const std::string& name) : ds_base::SensorBase(argc, argv, name), active_query(false), removed_header(false), mirroring(false), 
			channel_params_matched(false), board_params_matched(false), board_connection(NULL)
{
}

Trigger::~Trigger()
 {
 }

void Trigger::setupConnections()
{
  ds_base::DsProcess::setupConnections();

  // add connection to instrument and bind input to Trigger::parseRecievedBytes()
  board_connection = addConnection("instrument", boost::bind(&Trigger::parseReceivedBytes, this, _1));
}

void Trigger::setupPublishers()
{
  ds_base::SensorBase::setupPublishers();

  auto nh = nodeHandle();
  const auto name = ros::this_node::getName();

  auto check_params_topic = ros::param::param<std::string>("~check_params_topic", "board/params_matching");
  check_params_pub = nh.advertise<std_msgs::Bool>(name + "/" + check_params_topic, 1000);

  auto board_status_topic = ros::param::param<std::string>("~board_status_topic", "board/status");
  board_status_pub = nh.advertise<std_msgs::String>(name + "/" + board_status_topic, 1000);
  
}

void Trigger::setupParameters()
{
  ds_base::SensorBase::setupParameters();  // Setup the parameters

  // Informing the driver which parameters we want to compare (param server vs board's firmware)
  XmlRpc::XmlRpcValue bp;
  XmlRpc::XmlRpcValue chp;
  XmlRpc::XmlRpcValue conp;
  std::string fw_version;
  ros::param::get("~board_params", bp);
  ros::param::get("~channel_params", chp);
  ros::param::get("~configurable_params", conp);
  for (int q = 0; q < bp.size(); q++)
          board_params.push_back(bp[q]);
  for (int q = 0; q < chp.size(); q++)
          channel_params.push_back(chp[q]);
  for (int q = 0; q < conp.size(); q++)
	      configurable_params.push_back(conp[q]);

  // Get the character line terminator for queries sent to the trigger board
  ros::param::get("~fw_version", fw_version);
  if ('2' == fw_version.front() || '1' == fw_version.front() || '0' == fw_version.front())
  {
	  string_terminator = "\r\n";
  }
  else {
	  string_terminator = "\r";
  }

}

void Trigger::setupServices()
{
  ds_base::SensorBase::setupServices();

  auto nh = nodeHandle();

  // this binds any service request to the send command callback
  param_check_service = nh.advertiseService<ds_hotel_msgs::TrigPrmCtrlCmd::Request, ds_hotel_msgs::TrigPrmCtrlCmd::Response>(
      ros::this_node::getName() + "/" + "check_params", boost::bind(&Trigger::checkParameters, this, _1, _2));
  board_enablement_service = nh.advertiseService<std_srvs::SetBoolRequest, std_srvs::SetBoolResponse>(
      ros::this_node::getName() + "/" + "board_enablement", boost::bind(&Trigger::boardEnablementControl, this, _1, _2));
  board_configuration_service = nh.advertiseService<ds_hotel_msgs::TrigPrmCtrlCmd::Request, ds_hotel_msgs::TrigPrmCtrlCmd::Response>(
      ros::this_node::getName() + "/" + "install_configuration", boost::bind(&Trigger::installConfiguration, this, _1, _2));
  channel_enablement_service = nh.advertiseService<ds_hotel_msgs::TrigPrmCtrlCmd::Request, ds_hotel_msgs::TrigPrmCtrlCmd::Response>(
      ros::this_node::getName() + "/" + "channel_enablement", boost::bind(&Trigger::channelEnablementControl, this, _1, _2));
}

// Right now this is used to make sure the "command" field in req is the one designated for board configuration comparison (see checkParameters()). If that is the case,
// we send a query to the trigger board requesting that it send back its configuration data. 
bool Trigger::validateCommand(ds_hotel_msgs::TrigPrmCtrlCmd::Request& req, ds_hotel_msgs::TrigPrmCtrlCmd::Response& resp)
{
  // Make sure we are responding to a valid command.
  // This may not be necessary since each service is using a separate binded function.
  if (ds_hotel_msgs::TrigPrmCtrlCmd::Request::CHECK_PARAMS != req.command)
  {
    resp.is_successful = false;
    resp.reason = "Invalid query";
    return false;
  }
  // Query the trigger board (if its not already being queried)
  if (!active_query)
  {
    ROS_INFO("Sending a settings query to the trigger board");
    // let's query
    sendToBoard("settings");

    // Let the client know that we've sent their query to the board for processing
    resp.is_successful = false;
    resp.reason = "Query Initiated";

    // Can't forget to reinitialize state variables for the query processing
    mirroring = false;
    active_query = true;
    removed_header = false;
    json_payload.str(std::string());
    json_payload.clear();

    return false;
  }
  // If we're already processing a parameter checking query, let the user know we're working on it (used when clients try to spam the service). 
  if (active_query)
  {
	  resp.is_successful = false;
	  resp.reason = "Query in progress";
	  return false;
  }
  // If we've gotten to this point, we've ordered a legitimate status (or other) query and the trigger board is processing our request.
  ROS_INFO_STREAM("PASSED COMMAND VALIDATION");
  return true;
}

// To know what the trigger board's parameter configuration is, we have to first validate the JSON message that we got from it in 
// a previous call to checkParameters(). If we got a damaged or incomplete JSON message, the configuration data is no good.
bool Trigger::validateJson(ds_hotel_msgs::TrigPrmCtrlCmd::Response& resp)
{
  // If we've gotten to this point, we do have an active query. In that case, check if the JSON packet is valid
  bool valid_json = false;
  std::stringstream working_copy(json_payload.str());

  // Remove the "settings" header that precedes the JSON data (if it exists)
  if (json_payload.str().substr(0,sizeof("settings") - 1) == "settings")
  {  
    working_copy.str(std::string());
    ROS_INFO("Cleaning settings header from trigger board JSON data");
    working_copy << json_payload.str().erase(0,sizeof("settings") - 1); 
    removed_header = true;
  }
  
  // make sure we actually got a response from the trigger board
  if (working_copy.str().empty())
  {
      resp.is_successful = false;
      resp.reason = "No response from Trigger board";
      return false;
  }
  // make sure that the first and last non-whitespace characters are { and } (proper JSON)
  if (! (working_copy.str().at(working_copy.str().find_first_not_of(" \t\n\v\f\r")) == '{' && working_copy.str().at(working_copy.str().find_last_not_of(" \t\n\v\f\r")) == '}'))
  {
     valid_json = false;
     resp.is_successful = false;
     resp.reason = "Unrecognized JSON wrapper characters";
     return valid_json;
  }

  // make sure that we have an even number of left and right curly brackets 
  std::string t1 = working_copy.str();
  if (std::count(t1.begin(), t1.end(), '{') != std::count(t1.begin(), t1.end(), '}'))  
  {
     valid_json = false;
     resp.is_successful = false;
     resp.reason = "Mismatched JSON brackets";
     return valid_json;
  }
   
  // with those tests out of the way, let's parse the JSON.
  valid_json = reader.parse(working_copy, obj);

  // If the settings header is not removed yet 
  if(!valid_json)
  {
    ROS_WARN("Attempted to parse JSON data from trigger board");
    if (!removed_header)
      ROS_WARN("Possible lack of communication from trigger board");
    else 
      ROS_WARN("Header removed but JSON data still invalid");
    resp.is_successful = false;
    resp.reason = "JSON parse attempted";
    return false;
  }

  resp.is_successful = false;
  resp.reason = "Valid JSON";
  return valid_json;
}

// Callback for the "check_params" service that compares all the actual trigger board parameters to their param server counterparts. The ROSclient need only call "check_params"
// once to get a legitimate result! After calling this service once to send a query to the board, this node will 
// quickly return and later publish a true or false to <ds_trigger_node_namespace>/ds_trigger/board/params_matching. 
// Note that if you spam-call this service, it will ignore all calls until it completes the query (publishes a result). resp.is_successful will always return false, since the 
// ultimate say on whether the service was successful or not comes from the published result. 
bool Trigger::checkParameters(ds_hotel_msgs::TrigPrmCtrlCmd::Request& req, ds_hotel_msgs::TrigPrmCtrlCmd::Response& resp)
{
  bool continue_processing = false;
  board_params_matched = false;
  channel_params_matched = false;

  validateCommand(req, resp);

  // If we set "apply" to true in the request, we're not quite done here...
  if (req.apply)
  {
	mirroring = true;
  }
  
  return true;
}

// Configure the trigger board to be identical to the param server (i.e. every channel parameter and overall board parameter will be copied) 
void Trigger::mirrorParamServerToBoard(bool board_params_matched, bool channel_params_matched)
{
    
    XmlRpc::XmlRpcValue channels_config;
    ros::param::get(ros::names::resolve(ros::this_node::getName()) + "/channels", channels_config);
	// Because all of the individual channel configs are stored without labels under the "channels" group in the param server and don't have a 
	// simple data format for ros::param::get() to fetch them by, we can tell ROS to fetch the group as a generic XML type and then we'll interpret/cast it locally.
	// Here's a sample of what the channel configs could look like. Please see config/*.yaml for more details.
	// channels:
	//  - name: "DVL300"
	//     period: 500
	//     enabled: true
	//     polarity: 1                     // Each channel is homogeneous and anonymous
	//     etc....
	//   - name: "Pioneer"
	//     period: 500
	//     enabled: true
	//     polarity: 1
	//     etc...
	
    if (!board_params_matched)
    {
	sendGlobalParamsToBoard();
    }
    if (!channel_params_matched)
    {
	sendChannelParamsToBoard(channels_config);
    } 
}

// Compare all the overall board parameters like baudrate, oneshot, sync_polarity, etc. against the the param server.
bool Trigger::compareGeneralBoardParams(ds_hotel_msgs::TrigPrmCtrlCmd::Request& req,ds_hotel_msgs::TrigPrmCtrlCmd::Response& resp)
{
  std::string param_handle;
  int srvr_int;
  std::string srvr_string;
  bool srvr_bool;
  bool should_continue = true;

  // Loop through board parameters
  for (auto i : board_params)
  {
    // Fetch the parameter to check from parameter server: 
    param_handle = ros::names::resolve(ros::this_node::getName(), i);
       ROS_INFO_STREAM("CHECKING BOARD PARAMETER: " << param_handle);
	switch(obj[i].type()){
        case Json::intValue:  
          ros::param::get(param_handle, srvr_int);
          should_continue = srvr_int == obj[i].asInt() ? true : false;
          // Adding this exception for serial number since we don't care about it's value!
	 // if (std::string::npos != param_handle.find("serial_number"))
         // {
         //   should_continue = true;     //TO-DO: Verify that this isn't necessary anymore.
         // }
          break;
        case Json::stringValue:
          ros::param::get(param_handle, srvr_string);
          should_continue = srvr_string == obj[i].asString() ? true : false;
          break; 
        case Json::booleanValue: 
          ros::param::get(param_handle, srvr_bool);
          should_continue = srvr_bool == obj[i].asBool() ? true : false;
          break;
        default:
          ROS_ERROR("Detected an unsupported type in the trigger board's settings. Comparison not possible.");
	  resp.is_successful = false;
          resp.reason = "Unsupported type";
          should_continue = false; 
	  ROS_ERROR_STREAM("BAD TYPE IS: " << param_handle);
	  //return false;
          break;
      }
      // Now compare it to what the board gave us (handling different data types here)
      if (!should_continue)
      {
	    resp.is_successful = false;
	    resp.reason = "Parameter mismatch detected: ";
	    resp.reason.append(param_handle);
	    return false;
      }
    }
   
   return should_continue;
}

// Service to enable/disable the trigger board
bool Trigger::boardEnablementControl(std_srvs::SetBoolRequest& req, std_srvs::SetBoolResponse& resp)
{
  // "go" will set the board status to "running". "stop" will set the board status to "stopped".
    true == req.data ? sendToBoard("go") : sendToBoard("stop");
  
  // resp.message = "stopped/running" is a placeholder for gtest purposes. Longer term, I'd like to actually read status from the board following this command 
  // and verify that the board is "running" or "stopped".
  // Forcing a success here since we can't know for sure if the command succeeded until 
  // a response comes back from the board in the connection callback (parseReceivedBytes)
  true == req.data ? resp.message = "running" : resp.message = "stopped";
  resp.success = true;

  // Let's query the board to see if that command actually stuck
  sendToBoard("status");

  return true;
}

// Service to enable/disable individual channels on the trigger board
bool Trigger::channelEnablementControl(ds_hotel_msgs::TrigPrmCtrlCmd::Request& req, ds_hotel_msgs::TrigPrmCtrlCmd::Response& resp)
{

    std::string enablestr = "enable";
    std::string disablestr = "disable";
    
    //Data from the parameter server
    XmlRpc::XmlRpcValue channels_ref;
    ros::param::get(ros::this_node::getName() + "/channels", channels_ref);
    
    std::map<int, bool> command_params;

    // Loop through all the channels requested and map channel names to channel IDs.
    for (int i = 0; i < req.channels_to_change.size(); i++)
    {
	    for(int j = 0; j < channels_ref.size(); j++)
            {
		if (req.channels_to_change[i].name == (std::string)channels_ref[j]["name"])
		{
			command_params[j] = req.channels_to_change[i].enable;
		}
	    }
    }

    // If there's no channel with the provided name(s) found, notify the caller and return.
    if (0 == command_params.size())
    {
	resp.is_successful = false;
       	resp.reason = "Specified channel not found";
	return true;
    }
    for (auto pair : command_params)
    {
	    // build command string
	    std::stringstream cmdstr;
	    std::string opt = pair.second ? enablestr : disablestr;
	    cmdstr << "ch " << pair.first << " " << opt;
	
	    // send it
	    sendToBoard(cmdstr.str());
	    ros::Duration(0.2).sleep();

	    // Clear it just in case.
	    cmdstr.str().clear();
    }

    // let the caller know everything succeeded
    resp.is_successful = true;
    return true;
}

// Compare every parameter of every channel on the trigger board to the corresponding parameter on the param server.
bool Trigger::compareAllChannelParams(ds_hotel_msgs::TrigPrmCtrlCmd::Response& resp)
{
    std::string board_val;
    std::string server_val;
    std::stringstream delays_builder;
    bool should_continue = true; 
    bool run_checks = false;
    
    // Data from the board
    Json::Value& channels = obj["channels"];
    
    //Data from the parameter server
    XmlRpc::XmlRpcValue channels_ref;
    ros::param::get(ros::this_node::getName() + "/channels", channels_ref);

    for (int id = 0; id < channels.size(); id++)
    {  
	if (Json::ValueType::nullValue == channels[id].type())
	{
		//See if we're comparing to another null channel on the param server
		std::string sl((std::string)channels_ref[id]["name"]); 
		std::transform(sl.begin(), sl.end(), sl.begin(), ::tolower);
		if (sl == "null")
		{
			//everything is ok, channel # X is null on param server AND on the board, so we don't need to compare two empty channels
			ROS_INFO_STREAM("Skipping null channel");
			continue;
		}
	}
      for (auto cp : channel_params)
      {
        //ROS_INFO_STREAM("TESTING CHANNEL_PARAM " << cp << " on board channel " << channels[id]["name"].asString());
        switch(channels[id][cp].type()) {
          case Json::nullValue:
            ROS_ERROR_STREAM("Detected an null data type in trigger board channel settings.");
            resp.is_successful = false;
            resp.reason = "Null data type detected for parameter: ";
	          resp.reason.append(cp); 
            //return false;
	        break;
          case Json::intValue: 
            should_continue = channels[id][cp].asInt() == (int)channels_ref[id][cp] ? true : false;
          break;
          case Json::stringValue:
            // make sure to remove whitespace in both strings before comparison
            board_val = channels[id][cp].asString();
            server_val = (std::string)channels_ref[id][cp];
            board_val.erase(std::remove (board_val.begin(), board_val.end(), ' '), board_val.end());
            server_val.erase(std::remove (server_val.begin(), server_val.end(), ' '), server_val.end());
            should_continue = server_val == board_val ? true : false;
          break;
          case Json::booleanValue: 
            should_continue = channels[id][cp].asBool() == (bool)channels_ref[id][cp] ? true : false;
          break;
          case Json::arrayValue:
             // note: the json object that holds our data from the board treats "delays" as an array of ints, as does the parameter server 
	     ROS_INFO_STREAM("No. of DELAYS ON BOARD: " << channels[id][cp].size() << " No. of Delays ON PARAM SERVER " << channels_ref[id][cp].size());

	     if (channels[id][cp].size() != channels_ref[id][cp].size())
                    should_continue = false;
	     else {
                  for (int q = 0; q < channels_ref[id][cp].size(); q++)
	          {
			  if ((int)channels_ref[id][cp][q] != channels[id][cp][q].asInt())
			   {
				  should_continue = false;
				  break;
			   }
		  }
	     }
             
          break;
          default:
	          resp.is_successful = false;
            resp.reason = "Unsupported data type for parameter: ";
	          resp.reason.append(cp);
            return false;
          break;
        }
      	if (!should_continue)
      	{
	        resp.is_successful = false;
	        resp.reason = "Parameter mismatch detected: ";
	        resp.reason.append(cp);
	        return false;
      	}
      }
    }
    return should_continue;
}

// All Keystrings that the ROSclient specifies in the board configuration change request are string-string key-value pairs. With this function
// we convert those value strings to the proper datatype for upload to the parameter server. Strings for all values would be a good idea to avoid these conversions.
void Trigger::sendCastedGlobalsToServer(std::string ph, ds_core_msgs::KeyString pair)
{
  if (pair.value == "false" || pair.value == "true")
  {
    ros::param::set(ph, pair.value == "true" ? true: false);
    return;
  }
  
  try {
    int to_insert = std::stoi(pair.value);
    ros::param::set(ph, to_insert);
  }
  catch(...)
  {
    ROS_ERROR_STREAM("Attempted to configure trigger board parameter with invalid number.");
  }
}

// All Keystrings that the ROSclient specifies in the channel configuration change request are string-string key-value pairs. With this function
// we convert those value strings to the proper datatype for upload to the parameter server. Strings for all values would be a good idea to avoid these conversions.
void Trigger::sendCastedChannelParamsToServer(XmlRpc::XmlRpcValue &channels_ref, ds_core_msgs::KeyString pair)
{

  if (pair.value == "false" || pair.value == "true")
  {
    channels_ref[pair.key] = (pair.value == "true" ? true : false);
    return;
  }

  if (pair.key == "delays")
  {
    channels_ref[pair.key] = pair.value;
    return;
  }

  try {
    int to_insert = std::stoi(pair.value);
    channels_ref[pair.key] = to_insert;
    return;
  }
  catch(...)
  {
    ROS_ERROR_STREAM("Attempted to configure channel parameter with invalid number.");
    return;
  }

}

// Iterate through all the channels that the ROSclient wants to change and copy all the KeyString pairs of each of those channels to
// the corresponding channel on the parameter server
void Trigger::sendChannelParamsToServer(ds_hotel_msgs::TrigPrmCtrlCmd::Request& req, XmlRpc::XmlRpcValue &channels_ref)
{
    for (int k = 0; k < req.channels_to_change.size(); k++)
    {
      for (int i = 0; i < 8; i++)
      {
        if (!strcmp(req.channels_to_change[k].name.c_str(), static_cast<std::string>(channels_ref[i]["name"]).c_str()))
        {
          for (int l = 0; l < req.channels_to_change[k].specified_params.size(); l++)
          {
            // we found a channel that has the same name as the one we want to change- now let's change that channel (on the parameter server)  
            sendCastedChannelParamsToServer(channels_ref[i], req.channels_to_change[k].specified_params[l]);
          }
        }
      }       
    }
     ros::param::set(ros::names::resolve(ros::this_node::getName()) + "/channels", channels_ref);
}

// Send the configuration changes specified in "req" to the trigger board and update the parameter server. Like checkParameters(), the service call will
// set resp.is_successful to false since the final word on service success comes from the info published to <ds_trigger_node_namespace>/ds_trigger/board/matching_params
bool Trigger::installConfiguration(ds_hotel_msgs::TrigPrmCtrlCmd::Request& req, ds_hotel_msgs::TrigPrmCtrlCmd::Response& resp)
{
    std::stringstream command_builder;
    XmlRpc::XmlRpcValue channels_ref;
    std::string param_handle;
    
    // pull channel configurations from parameter server
    ros::param::get(ros::names::resolve(ros::this_node::getName()) + "/channels", channels_ref);

    // load board parameters onto the parameter server
    for (int q = 0; q < req.global_params_to_change.size(); q++)
    {
      param_handle = ros::names::resolve(ros::this_node::getName(), req.global_params_to_change.at(q).key);
      sendCastedGlobalsToServer(param_handle, req.global_params_to_change.at(q));
    }

    // load channel parameters onto the parameter server
    sendChannelParamsToServer(req, channels_ref);

    // Load the channel configuration objects (from the param server) onto the trigger board
    sendChannelParamsToBoard(channels_ref);

    // Load the global board config values (from the param server) onto the trigger board
    sendGlobalParamsToBoard();

    resp.is_successful = false;

    return true;
}

// Copy the channel-configurations from the parameter server to the trigger board.
void Trigger::sendChannelParamsToBoard(XmlRpc::XmlRpcValue channels_config)
{
  std::stringstream command_builder;
  // load channel parameters onto the parameter server	
  for (int i = 0; i < 8; i++)
  {
    command_builder << "ch " << i << " clear" << "\r";
    //usleep(100000);
    command_builder.str(std::string());
    command_builder.clear();
    for (auto cp : channel_params)
    {
      // create a command string:
        command_builder << "ch " << i << " " << cp.c_str() << " = " << channels_config[i][cp];
      // Now send that to the board!
      // Add a slight delay for each command so we don't overload the board. 
        usleep(100000);
        sendToBoard(command_builder.str());
        std::cout << command_builder.str() << std::endl;
      // Now clear that command string!!!
      command_builder.str(std::string());
      command_builder.clear();
    }
  }
  // Now save that configuration to the board's non-volatile memory.
    sendToBoard("save"); 

}

// Copy the overall board parameters from the parameter server to the trigger board.
void Trigger::sendGlobalParamsToBoard()
{
  std::stringstream command_builder;
  std::string param_handle;

  XmlRpc::XmlRpcValue board_param_xmlrpc_temp;
  for (auto i : configurable_params)
  {
    auto cmd = ds_hotel_msgs::TrigPrmCtrlCmd{};
    cmd.request.command = ds_hotel_msgs::TrigPrmCtrlCmd::Request::ENABLE_BOARD;
    // Fetch the parameter to check from parameter server: 
    param_handle = ros::names::resolve(ros::this_node::getName(), i);
    ros::param::get(param_handle, board_param_xmlrpc_temp);
    if (XmlRpc::XmlRpcValue::Type::TypeInt == board_param_xmlrpc_temp.getType())
    {  
      command_builder << i << " = " << (int)board_param_xmlrpc_temp;
    }
    if (XmlRpc::XmlRpcValue::Type::TypeBoolean == board_param_xmlrpc_temp.getType())
    {
      command_builder << i << " " << ((bool)board_param_xmlrpc_temp == true ? "enable" : "disable");
    }
    // And send it to the board!
     sendToBoard(command_builder.str());
     std::cout << command_builder.str() << std::endl;
    // Now clear out that old command 
    command_builder.str(std::string());
    command_builder.clear();
  }
  // Now save that configuration to the board's non-volatile memory.
  sendToBoard("save");
	
}

void Trigger::sendToBoard(std::string cmdstr)
{
    if (board_connection)
    {
      board_connection->send(cmdstr + string_terminator);
    }
}

// process the data in the response from the trigger board.
void Trigger::parseReceivedBytes(const ds_core_msgs::RawData& bytes)
{
  // clear the buffer we store json data from the board in 
  obj.clear();
  json_payload.str(std::string());
  json_payload.clear();
  
  for (auto data: bytes.data)
  {
    json_payload << data;
  }
 
  // Trim the ETX from the arriving message
  if ('\x03' == json_payload.str().back()) 
  {
    std::string holder(json_payload.str());
    holder.pop_back();
    json_payload.str(holder);
  }

  // Feedback from the board regarding how checkParameters went (initial response)
  reportCheckParametersSuccess();
  
  // Feedback from the board regarding how checkParameters went (if "apply=true" in checkParameters() servicecall)
  // mirroring will be true here
  reportMirroringSuccess();
  
  // Feedback from the board regarding whether the board is enabled ("running" or "stopped")
  reportBoardEnabledStatus();
}

// Publish whether the trigger board's configuration matches that of the param server to the topic 
// <ds_trigger_node_namespace>/ds_trigger/board/params_matching.
void Trigger::reportCheckParametersSuccess()
{
  ds_hotel_msgs::TrigPrmCtrlCmd::Request req;
  ds_hotel_msgs::TrigPrmCtrlCmd::Response resp;
  std_msgs::Bool b;

  if (active_query && (json_payload.str().substr(0,sizeof("settings") - 1) == "settings"))
  {
	// gotta strip the output first....
	validateJson(resp);
	board_params_matched = compareGeneralBoardParams(req, resp);
        channel_params_matched = compareAllChannelParams(resp);
	if (board_params_matched && channel_params_matched)
	{
	  // publish accordingly
	  // Whether apply is true or not, we don't care here. If param server matches board, we return true- that's just that!
	  b.data = true;
	  check_params_pub.publish(b);
	} 
	// if we have a mismatch somewhere, we need to publish accordingly
	if (!board_params_matched || !channel_params_matched)
	{
		// If there is a mismatch between param server and the board and the serviceclient called CheckParameters with "apply" set to true
		// let's make the board resemble the param server.
		if (mirroring) 
		{
			mirrorParamServerToBoard(board_params_matched, channel_params_matched);
			return;
		}
	  // If there's a mismatch and we haven't tried to force a match with "apply", just publish to the client that we have a failure.
	  b.data = false;
	  check_params_pub.publish(b);

	}
        
	// Clean out the state variables for checkParameters, but ONLY if we aren't mirroring to the board.
	// If we are mirroring to the board (i.e., the client passed apply=true to checkParameters), we will get
	// a return message from the board confirming whether or not the reconfiguration went through
	if (!mirroring)
	{
	  markCheckParametersCompleted();
	}
  }
}
// Publish whether the services "install_configuration" or "check_params" (with "apply=true") succeeded or not, i.e., 
// were we able to clone client/param-server configuration to the trigger board.
// The topic published on is <ds_trigger_node_namespace>/ds_trigger/board/matching_params.
void Trigger::reportMirroringSuccess()
{
  Json::Value json_holder;
  std_msgs::Bool b;
  if (active_query && mirroring)
  {
    std::string holder(json_payload.str());
    holder.erase(0, holder.find_first_of('{'));
    json_payload.str(holder);
    reader.parse(holder, json_holder);
    if (json_holder.isMember("error_code") && json_holder.isMember("error_msg")) 
    {
      if ((0 == json_holder["error_code"].asInt()) && ("success" == json_holder["error_msg"].asString()))
      {
	// the mirroring has worked. publish the success accordingly and mark the query as completed.
	  b.data = true;
	  check_params_pub.publish(b);
	  markCheckParametersCompleted();
      }
    }
  }
}

// Clear out the state variables used in doing the check_params service.
void Trigger::markCheckParametersCompleted()
{
  active_query = false;
  removed_header = false;
  json_payload.str(std::string());
  json_payload.clear();
  mirroring = false;
}

// Publish whether the trigger board is enabled or not, i.e. is it triggering pings or not. This publishing will happen whenever the "board_enablement" service is called.
// The topic published on is <ds_trigger_node_namespace>/ds_trigger/board/status.
void Trigger::reportBoardEnabledStatus()
{
  std::stringstream temp;
  if(json_payload.str().substr(0,sizeof("status") - 1) == "status")
  {
    temp.str(std::string());
    temp << json_payload.str().erase(0,sizeof("settings") - 1); 
    reader.parse(temp, obj);
    std_msgs::String s;
    s.data = obj["status"].asString();
    board_status_pub.publish(s);
  }
}

}

